// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Preforms the category autocomplete search for the rule editing form.
 *
 * This module is compatible with core/form-autocomplete.
 *
 * @module     tool_roleremoval/category_search
 * @copyright  20201 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

import * as Ajax from 'core/ajax';
import * as Notification from 'core/notification';

/**
 * Process the results for auto complete elements.
 *
 * @param {String} selector The selector of the auto complete element.
 * @param {Array} results An array or results.
 * @return {Array} New array of results.
 */
export const processResults = (selector, results) => {
    let options = [];

    results.contexts.forEach((result) => {
        options.push(
            {
                value: result.id,
                label: result.name
            }
        );
    });

    return options;
};

/**
 * Source of data for Ajax element.
 *
 * @param {String} selector The selector of the auto complete element.
 * @param {String} query The query string.
 * @param {Function} callback A callback function receiving an array of results.
 */
export const transport = (selector, query, callback) => {
    // The parameters for the AJAX call.
    let params = {
        methodname: 'tool_roleremoval_search_context',
        args: {
            query: query
        }
    };

    Ajax.call([params])[0].then(callback).catch(Notification.exception);
};
