<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The page for manually triggering a rule.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core\output\notification;
use tool_roleremoval\rule;
use tool_roleremoval\task\apply_rule;

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->libdir.'/adminlib.php');

$id = optional_param('id', 0, PARAM_INT);
$confirm = optional_param('confirm', false, PARAM_BOOL);

$pageparams = [];

if (!empty($id)) {
    $pageparams['id'] = $id;
}

$pageurl = '/' . $CFG->admin . '/tool/roleremoval/triggerrule.php';

admin_externalpage_setup('toolroleremoval', '', $pageparams, $pageurl);

$rule = rule::get_rule($id);

if (!$rule->valid()) {
    // Invalid rules will not run.
    redirect(new moodle_url('/' . $CFG->admin . '/tool/roleremoval/index.php'));
}

$returnurl = new moodle_url('/' . $CFG->admin . '/tool/roleremoval/viewrule.php', ['id' => $id]);

if ($rule->enabled()) {
    // Automatic rules cannot be triggered manually.
    redirect(new moodle_url($returnurl));
}

if ($confirm) {
    require_sesskey();

    try {
        apply_rule::trigger($rule);
    } catch (\moodle_exception $e) {
        redirect($returnurl, $e->getMessage(), 0, notification::NOTIFY_ERROR);
    }

    $message = get_string('ruletriggered', 'tool_roleremoval', format_string($rule->rulename));
    redirect($returnurl, $message, 0, notification::NOTIFY_SUCCESS);
}

/* @global \core_renderer $OUTPUT The global renderer. */
echo $OUTPUT->header();
$messageparams = [
    'context' => $rule->get_context()->get_context_name(),
    'inactivefor' => format_time($rule->inactivefor),
    'notice' => format_time($rule->notice),
    'role' => $rule->get_rolename(),
    'rulename' => format_string($rule->rulename),
];
$message = get_string('triggerrule', 'tool_roleremoval', $messageparams);
$confirmurl = new moodle_url($pageurl, ['id' => $id, 'confirm' => 1]);
echo $OUTPUT->confirm($message, $confirmurl, $returnurl);
echo $OUTPUT->footer();
