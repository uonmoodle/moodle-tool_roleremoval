<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Configure the settings for the role revocation tool
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $testurl = new moodle_url('/admin/tool/roleremoval/preview_messages.php');

    $settings = new admin_settingpage('tool_roleremoval', get_string('pluginname', 'tool_roleremoval'));
    $ADMIN->add('tools', $settings);

    $link = html_writer::link($testurl, get_string('testmessages', 'tool_roleremoval'));
    $settings->add(
        new admin_setting_heading(
            'dbsettings',
            get_string('messagesettings', 'tool_roleremoval'),
            get_string('messagesettings_help', 'tool_roleremoval', $link)
        )
    );

    $settings->add(
        new admin_setting_configtextarea(
            'tool_roleremoval/notifysignature',
            get_string('notifysignature', 'tool_roleremoval'),
            get_string('notifysignature_help', 'tool_roleremoval'),
            '',
            PARAM_RAW_TRIMMED
        )
    );
    $settings->add(
        new admin_setting_configtextarea(
            'tool_roleremoval/revocationsignature',
            get_string('revocationsignature', 'tool_roleremoval'),
            get_string('revocationsignature_help', 'tool_roleremoval'),
            '',
            PARAM_RAW_TRIMMED
        )
    );

    $test = new admin_externalpage(
        'toolroleremovaltestmessages',
        get_string('testmessages', 'tool_roleremoval'),
        $testurl,
        'moodle/site:config',
        true
    );
    $ADMIN->add('tools', $test);

    $ADMIN->add(
        'roles',
        new admin_externalpage(
            'toolroleremoval',
            get_string('pluginname', 'tool_roleremoval'),
            $CFG->wwwroot.'/'.$CFG->admin.'/tool/roleremoval/index.php',
            'moodle/site:config'
        )
    );
}
