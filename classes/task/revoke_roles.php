<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\task;

use context;
use context_helper;
use tool_roleremoval\messenger;

/**
 * Revokes roles from users who have still not logged in
 * after their notification period is finished.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class revoke_roles extends \core\task\scheduled_task {
    /**
     * @inheritDoc
     */
    public function get_name() {
        return get_string('task:revokeroles', 'tool_roleremoval');
    }

    /**
     * @inheritDoc
     */
    public function execute() {
        mtrace('Cleaning queue... ', '');
        $this->cleanup_queue();
        mtrace('done.');

        $roles = role_get_names();

        while ($byuser = $this->get_revocations()) {
            // If there are very many revocations to process we will only do 2000 at a time.
            foreach ($byuser as $userid => $user) {
                mtrace('Revoking roles for user ' . $userid . '... ', '');
                $contextroles = [];
                foreach ($user as $revocation) {
                    // Remove the role from the user.
                    role_unassign($revocation->roleid, $revocation->id, $revocation->contextid);
                    $this->mark_as_done($revocation->qid);

                    // Prepare the information about the role removed so we can message the user about it.
                    $ctxid = $revocation->ctxid;
                    context_helper::preload_from_record($revocation);
                    $context = context::instance_by_id($ctxid);

                    $contextroles[] = [
                        'name' => $context->get_context_name(),
                        'role' => $roles[$revocation->roleid]->localname,
                    ];
                }

                // Message the user about all the roles that were revoked.
                messenger::revocation($user[0], $contextroles);
                mtrace('done.');
            }
        }
    }

    /**
     * Marks the queue item as completed.
     *
     * @param $id The id of the queue record.
     * @returns void
     */
    protected function mark_as_done($id) {
        global $DB;
        $DB->delete_records('tool_roleremoval_queue', ['id' => $id]);
    }

    /**
     * Removes all records from the queue that are no longer valid.
     *
     * @return void
     */
    protected function cleanup_queue() {
        $this->remove_loggedin_users();
        $this->remove_invalid_contexts();
        $this->remove_invalid_roles();
    }

    /**
     * Remove all revocations where the user has logged in since it was created.
     *
     * @return void
     */
    protected function remove_loggedin_users() {
        global $DB;

        $sql = "SELECT q.id
                 FROM {tool_roleremoval_queue} q
                 JOIN {user} u ON u.id = q.userid
                WHERE q.created < u.lastaccess";

        $DB->delete_records_subquery('tool_roleremoval_queue', 'id', 'id', $sql);
    }

    /**
     * Remove all revocations where the context no longer exists.
     *
     * @return void
     */
    protected function remove_invalid_contexts() {
        global $DB;

        $sql = "SELECT q.id
                 FROM {tool_roleremoval_queue} q
            LEFT JOIN {context} c ON c.id = q.contextid
                WHERE c.id IS NULL";

        $DB->delete_records_subquery('tool_roleremoval_queue', 'id', 'id', $sql);
    }

    /**
     * Remove all revocations where the role no longer exists.
     *
     * @return void
     */
    protected function remove_invalid_roles() {
        global $DB;

        $sql = "SELECT q.id
                 FROM {tool_roleremoval_queue} q
            LEFT JOIN {role} r ON r.id = q.roleid
                WHERE r.id IS NULL";

        $DB->delete_records_subquery('tool_roleremoval_queue', 'id', 'id', $sql);
    }

    /**
     * Gets a list revocation actions to be completed per user.
     *
     * Note the total number of records we fetch is limited to 2000.
     *
     * @return array
     * @throws \dml_exception
     */
    protected function get_revocations() {
        global $DB;

        $params = [
            'time' => time(),
        ];

        $contextfields = context_helper::get_preload_record_columns_sql('ctx');

        $sql = "SELECT q.id AS qid, q.contextid, q.roleid, u.*, $contextfields
                 FROM {tool_roleremoval_queue} q
                 JOIN {user} u ON u.id = q.userid
                 JOIN {context} ctx ON ctx.id = q.contextid
                WHERE q.revokeon < :time";

        $revocations = $DB->get_records_sql($sql, $params, 0, 2000);

        $byusers = [];

        foreach ($revocations as $revocation) {
            if (!isset($byusers[$revocation->id])) {
                $byusers[$revocation->id] = [];
            }

            $byusers[$revocation->id][] = $revocation;
        }

        return $byusers;
    }
}
