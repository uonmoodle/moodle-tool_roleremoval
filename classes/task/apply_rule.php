<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\task;

use core\task\manager;
use moodle_exception;
use moodle_url;
use tool_roleremoval\rule;

/**
 * Runs a manual rule.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class apply_rule extends \core\task\adhoc_task {
    /**
     * @inheritDoc
     */
    public function execute() {
        $data = $this->get_custom_data();
        try {
            $rule = rule::get_rule($data->rule);
        } catch (\dml_missing_record_exception $e) {
            mtrace('Rule ' . $data->rule . ' has been deleted.');
            return;
        }
        mtrace('Processing rule: ' . $rule->rulename);
        $rule->apply();
    }

    /**
     * Returns the task name.
     *
     * @return string
     */
    public function get_name() {
        return get_string('task:applyrule', 'tool_roleremoval');
    }
    
    /**
     * Triggers a run of a rule.
     *
     * @param \tool_roleremoval\rule $rule
     * @throws \moodle_exception When there is a duplicate run for the rule.
     */
    public static function trigger(rule $rule) {
        $task = new apply_rule();
        $taskdata = (object) array(
            'rule' => $rule->get_id(),
        );
        $task->set_custom_data($taskdata);
        // We will not allow a rule to have multiple jobs set to run at the same time.
        $saved = manager::queue_adhoc_task($task, true);

        if (!$saved) {
            throw new moodle_exception('task_apply_rule_duplicate', 'tool_roleremoval');
        }
    }
}
