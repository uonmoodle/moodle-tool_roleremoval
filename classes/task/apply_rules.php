<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\task;

use tool_roleremoval\rule;

/**
 * Runs all automatic rules.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class apply_rules extends \core\task\scheduled_task {
    /**
     * @inheritDoc
     */
    public function get_name() {
        return get_string('task:applyrules', 'tool_roleremoval');
    }

    /**
     * @inheritDoc
     */
    public function execute() {
        mtrace('Fetching rules... ', '');
        $list = rule::get_rules(true);
        mtrace(count($list->rules) . ' rules found.');

        foreach ($list->rules as $rule) {
            mtrace('Processing rule: ' . $rule->rulename);
            $rule->apply();
        }
    }
}
