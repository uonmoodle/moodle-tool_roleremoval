<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\output;

use coding_exception;
use context_system;
use dml_missing_record_exception;
use moodle_url;
use renderer_base;
use tool_roleremoval\rule;

/**
 * A list of rules.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rules_list implements \templatable, \renderable {
    /** @var rule[] A list of rules. */
    public $rules = [];

    /**
     * @inheritDoc
     */
    public function export_for_template(renderer_base $output) {
        global $CFG;

        $deletepage = '/' . $CFG->admin . '/tool/roleremoval/deleterule.php';
        $editpage = '/' . $CFG->admin . '/tool/roleremoval/editrule.php';
        $viewpage = '/' . $CFG->admin . '/tool/roleremoval/viewrule.php';

        $list = (object) [
            'canedit' => has_capability('tool/roleremoval:edit', context_system::instance()),
            'newrule' => new moodle_url($editpage),
            'rules' => [],
        ];

        foreach ($this->rules as $rule) {
            if (!is_a($rule, rule::class)) {
                throw new coding_exception('Rules must be an instance of \tool_rolereplace\rule');
            }

            $data = (object)(array)$rule;
            $data->id = $rule->get_id();
            $data->valid = $rule->valid();

            // Get the full context details.
            try {
                $context = $rule->get_context();
                $data->contextname = $context->get_context_name();
                $data->contexturl = $context->get_url();
            } catch (dml_missing_record_exception $e) {
                // The context is invalid.
                $data->contextname = get_string('invalidcontext', 'tool_roleremoval');
                $data->contexturl = new moodle_url('/');
            }

            // Get the localised role name.
            $data->rolename = $rule->get_rolename();

            // Timings.
            $data->inactivefor = format_time($rule->inactivefor);
            $data->notice = format_time($rule->notice);

            // Add the urls for the rule.
            $data->deleteurl = new moodle_url($deletepage, ['id' => $rule->get_id()]);
            $data->editurl = new moodle_url($editpage, ['id' => $rule->get_id()]);
            $data->viewurl = new moodle_url($viewpage, ['id' => $rule->get_id()]);

            $list->rules[] = $data;
        }

        return $list;
    }
}
