<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\output;

use context;
use context_helper;
use moodle_url;
use renderer_base;

/**
 * A list of users who match a rule.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class user_list implements \templatable, \renderable {
    /** The number of user records to display at one time. */
    const LIMIT = 50;

    /** @var \tool_roleremoval\rule The rule that the user list if for. */
    public $rule;

    /** @var int The page of results to display. */
    public $page = 1;

    /**
     * @inheritDoc
     */
    public function export_for_template(renderer_base $output) {
        $start = static::LIMIT * ($this->page - 1);
        $matches = $this->rule->get_matches($start, static::LIMIT);

        $usermatches = [];

        foreach ($matches as $match) {
            $ctxid = $match->ctxid;
            context_helper::preload_from_record($match);
            $context = context::instance_by_id($ctxid);

            $usermatches[] = (object)[
                'id' => $match->id,
                'name' => fullname($match),
                'lastaccess' => $match->lastaccess,
                'contexturl' => $context->get_url(),
                'contextname' => $context->get_context_name(),
                'profileurl' => new moodle_url('/user/index.php', ['id' => $match->id]),
            ];
        }

        return (object)[
            'enabled' => $this->rule->enabled(),
            'exporturl' => new moodle_url('exportmatches.php', ['id' => $this->rule->get_id()]),
            'matches' => $usermatches,
            'rulename' => $this->rule->rulename,
            'runurl' => new moodle_url('triggerrule.php', ['id' => $this->rule->get_id()]),
            'valid' => $this->rule->valid(),
            'notice' => format_time($this->rule->notice),
            'role' => $this->rule->get_rolename(),
        ];
    }
}
