<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use context;
use context_helper;
use context_system;
use stdClass;

/**
 * Class used to export data from the plugin.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class export {
    /** @var array Cached extra username field data for the user. */
    protected static $usernamefields;

    /**
     * Formats a record returned by tool_roleremoval\rule::get_matches for export.
     *
     * @param \stdClass $record
     * @return array
     */
    public static function format_match_record(stdClass $record): array {
        $data = new stdClass();

        $data->fullname = fullname($record);

        // Pull out the identity fields the user can access.
        $extrafields = static::get_extra_user_fields();
        foreach ($extrafields as $field) {
            $data->$field = $record->$field;
        }

        // Add in the last access date.
        $data->lastaccess = userdate($record->lastaccess);

        // Add in the context information.
        $ctxid = $record->ctxid;
        context_helper::preload_from_record($record);
        $context = context::instance_by_id($ctxid);

        $data->contextname = $context->get_context_name();
        $data->contexturl = $context->get_url()->out(false);

        return (array) $data;
    }

    /**
     * Gets the extra username fields accessible to the user.
     *
     * @return array
     */
    protected static function get_extra_user_fields(): array {
        if (!isset(static::$usernamefields)) {
            static::$usernamefields = \core_user\fields::for_identity(context_system::instance())
                ->excluding('lastaccess')
                ->get_required_fields();
        }
        return static::$usernamefields;
    }
}
