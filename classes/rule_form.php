<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use context;
use dml_missing_record_exception;

/**
 * The form for editing of rules.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rule_form extends \moodleform {
    /**
     * @inheritDoc
     */
    protected function definition() {
        $mform = $this->_form;

        // Display form title.
        $mform->addElement('header', 'general', get_string('editrule', 'tool_roleremoval'));

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);

        // A name for the rule.
        $mform->addElement('text', 'rulename', get_string('rulename', 'tool_roleremoval'));
        $mform->setType('rulename', PARAM_TEXT);
        $mform->addRule('rulename', get_string('required'), 'required');
        $mform->addHelpButton('rulename', 'rulename', 'tool_roleremoval');

        // We get the contexts via AJAX only (otherwise this form is likely to take minutes to load).
        $mform->addElement(
            'autocomplete',
            'contextid',
            get_string('contextid', 'tool_roleremoval'),
            [],
            [
                'ajax' => 'tool_roleremoval/category_search',
                'valuehtmlcallback' => [$this, 'display_value'],
            ]
        );
        $mform->setType('contextid', PARAM_INT);
        $mform->addRule('contextid', get_string('required'), 'required');
        $mform->addHelpButton('contextid', 'contextid', 'tool_roleremoval');

        // List all the roles, we will check if they are valid for the context during saving.
        $roles = role_get_names();
        $rolelist = [];
        foreach ($roles as $role) {
            $rolelist[$role->id] = $role->localname;
        }

        $mform->addElement(
            'select',
            'roleid',
            get_string('roleid', 'tool_roleremoval'),
            [0 => ''] + $rolelist
        );
        $mform->setType('roleid', PARAM_INT);
        $mform->addRule('roleid', get_string('required'), 'required');
        $mform->addHelpButton('roleid', 'roleid', 'tool_roleremoval');

        // How long a user is inactive for before the rule activates for them.
        $mform->addElement('duration', 'inactivefor', get_string('inactivefor', 'tool_roleremoval'));
        $mform->addHelpButton('inactivefor', 'inactivefor', 'tool_roleremoval');
        $mform->setDefault('inactivefor', YEARSECS);

        // The amount of time we give a user to log in after wthe rule triggers.
        $mform->addElement('duration', 'notice', get_string('notice', 'tool_roleremoval'));
        $mform->addHelpButton('notice', 'notice', 'tool_roleremoval');
        $mform->setDefault('notice', WEEKSECS * 4);

        // Enables the rule.
        $mform->addElement('checkbox', 'enabled', get_string('ruleenabled', 'tool_roleremoval'));
        $mform->setType('enabled', PARAM_TEXT);
        $mform->addHelpButton('enabled', 'ruleenabled', 'tool_roleremoval');

        $this->add_action_buttons();
    }

    /**
     * Callback method to render the name of the selected context the form.
     *
     * @param int $value The id of the context.
     * @return string the display name of the context.
     * @throws \coding_exception
     */
    public function display_value(int $value): string {
        if (empty($value)) {
            // When the form is saved with no value set we get 0 passed,
            // this will cause a coding_exception when fetching the context.
            return get_string('invalidcontext', 'tool_roleremoval');
        }

        try {
            $context = context::instance_by_id($value);
            $name = $context->get_context_name();
        } catch (dml_missing_record_exception $e) {
            $name = get_string('invalidcontext', 'tool_roleremoval');
        }
        return $name;
    }

    /**
     * Saves the data in the form.
     */
    public function save() {
        $data = $this->get_data();
        $rule = rule::create_rule($data);
        $rule->save();
    }

    /**
     * @inheritDoc
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        $rule = rule::create_rule((object)$data);

        try {
            $rule->get_context();
        } catch (\Exception $e) {
            $errors['contextid'] = get_string('invalidcontext', 'tool_roleremoval');
        }

        // Validate that the role is valid.
        if (!$rule->valid_role()) {
            $errors['roleid'] = get_string('invalidrole', 'tool_roleremoval');
        }

        return $errors;
    }
}
