<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use context;
use context_system;
use context_helper;
use core_course_category;
use context_coursecat;

/**
 * Class that searches for items relevant to the tool.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class search {
    /**
     * Searches system, category and course contexts.
     *
     * The results will be returned containing objects in the following structure:
     *  {
     *      id: The context id
     *      name: The name of the context.
     *  }
     *
     * @param string $query The term that should be in the name of the context.
     * @param int $limit The maximum number of results to return.
     * @return array
     */
    public static function context(string $query, int $limit = 20): array {
        global $DB;
        $params = [
            'catlevel' => CONTEXT_COURSECAT,
            'catquery' => "%$query%",
            'courselevel' => CONTEXT_COURSE,
            'coursequery' => "%$query%",
        ];

        $results = [];

        // We cannot check the name of the system context in the database since it has a localised string.
        $systemcontext = context_system::instance();

        if (empty($query)) {
            // Return early if there is no query.
            $results[] = (object) [
                'id' => $systemcontext->id,
                'name' => $systemcontext->get_context_name(),
            ];
            return $results;
        }

        if (strpos($systemcontext->get_context_name(), $query) !== false) {
            $results[] = (object) [
                'id' => $systemcontext->id,
                'name' => $systemcontext->get_context_name(),
            ];
            // We need one less result from the rest of the query.
            $limit--;
        }

        // We want to preload the contexts to reduce the number of database calls when we get their names.
        $fields = context_helper::get_preload_record_columns_sql('ctx');

        // Query to search category names.
        $catlike = $DB->sql_like('cat.name', ':catquery', false, false);
        $categorycontext = "SELECT ctx.id, $fields
                            FROM {context} ctx
                            JOIN {course_categories} cat ON cat.id = ctx.instanceid AND ctx.contextlevel = :catlevel
                           WHERE $catlike";

        // Query to search course full names.
        $courselike = $DB->sql_like('c.fullname', ':coursequery', false, false);
        $coursecontext = "SELECT  ctx.id, $fields
                            FROM {context} ctx
                            JOIN {course} c ON c.id = ctx.instanceid AND ctx.contextlevel = :courselevel
                           WHERE $courselike";

        // Do the query and then get the name of the item.
        $sql = "$categorycontext UNION $coursecontext";

        $records = $DB->get_records_sql($sql, $params, 0, $limit);



        foreach ($records as $record) {
            context_helper::preload_from_record($record);
            $context = context::instance_by_id($record->id);

            if ($context->contextlevel == CONTEXT_COURSECAT) {
                $categoryname = core_course_category::get($context->instanceid)->get_nested_name(false," / ");
                $contextname = context_coursecat::get_level_name() . ": ". $categoryname;
            } else {
                $contextname = $context->get_context_name();
            }

            $results[] = (object) [
                'id' => $context->id,
                'name' => $contextname,
            ];
        }

        return $results;
    }
}
