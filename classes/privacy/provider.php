<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\privacy;

use core_privacy\local\metadata\collection;
use core_privacy\local\request\approved_contextlist;
use core_privacy\local\request\approved_userlist;
use core_privacy\local\request\context;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\transform;
use core_privacy\local\request\userlist;
use core_privacy\local\request\writer;

/**
 * Definition of the data that is stored by the plugin.
 *
 * @see https://docs.moodle.org/dev/Privacy_API
 *
 * @package    tool_roleremoval
 * @category   privacy
 * @copyright  2021 Nottingham University
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements
    \core_privacy\local\metadata\provider,
    \core_privacy\local\request\core_userlist_provider,
    \core_privacy\local\request\plugin\provider {
    /**
     * Returns metadata about the role revocation plugin.
     *
     * @param \core_privacy\local\metadata\collection $collection The initialised collection to add items to.
     * @return \core_privacy\local\metadata\collection A listing of user data stored through this system.
     */
    public static function get_metadata(collection $collection): collection {
        $collection->add_subsystem_link('core_message', [], 'privacy:metadata:core_message');

        $queue = [
            'userid' => 'privacy:metadata:tool_roleremoval_queue:userid',
            'contextid' => 'privacy:metadata:tool_roleremoval_queue:contextid',
            'roleid' => 'privacy:metadata:tool_roleremoval_queue:roleid',
            'revokeon' => 'privacy:metadata:tool_roleremoval_queue:revokeon',
            'created' => 'privacy:metadata:tool_roleremoval_queue:created',
        ];
        $collection->add_database_table('tool_roleremoval_queue', $queue, 'privacy:metadata:tool_roleremoval_queue');

        return $collection;
    }

    /**
     * Finds out the contexts on which a user has roles scheduled for revocation.
     *
     * @param int $userid
     * @return \core_privacy\local\request\contextlist
     */
    public static function get_contexts_for_userid(int $userid): contextlist {
        $contextlist = new contextlist();

        // We need to get all the contexts the user is due to have their role revoked on separately as they
        // could just be having their data from a sub context be deleted.
        $sql = "SELECT c.id
                 FROM {context} c
                 JOIN {tool_roleremoval_queue} q ON q.contextid = c.id
                WHERE q.userid = :userid";
        $contextlist->add_from_sql($sql, ['userid' => $userid]);

        return $contextlist;
    }

    /**
     * Exports role revocation instructions that will affect each context.
     *
     * @param \core_privacy\local\request\approved_contextlist $contextlist
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        global $DB;

        if (empty($contextlist)) {
            return;
        }

        $user = $contextlist->get_user();
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        $sql = "SELECT q.*, c.id AS ctxid
                  FROM {context} c
                  JOIN {tool_roleremoval_queue} q ON q.contextid = c.id
                 WHERE q.userid = :userid AND c.id $contextsql";
        $params = [
            'userid' => $user->id,
        ];
        $params += $contextparams;
        $queueitems = $DB->get_recordset_sql($sql, $params);

        $roles = get_all_roles();

        $subcontext = get_string('privacy:export:queueitem', 'tool_roleremoval');
        foreach ($queueitems as $item) {
            $context = \context::instance_by_id($item->ctxid);

            if (isset($roles[$item->roleid])) {
                $rolename = role_get_name($roles[$item->roleid], $context, ROLENAME_BOTH);
            } else {
                $rolename = get_string('invalidrole', 'tool_roleremoval');
            }

            $itemcontext = "$item->roleid - $rolename";

            $itemdata = (object) [
                'revokeon' => transform::datetime($item->revokeon),
                'created' => transform::datetime($item->created),
            ];
            writer::with_context($context)->export_data([$subcontext, $itemcontext], $itemdata);
        }
    }

    /**
     * Deletes the role revocations for the user in this context.
     *
     * The expectation is that their role will also be removed by other
     * parts of the privacy API.
     *
     * @param \context $context
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        global $DB;
        $DB->delete_records('tool_roleremoval_queue', ['contextid' => $context->id]);
    }

    /**
     * Deletes all data for a user from approved contexts.
     *
     * @param \core_privacy\local\request\approved_contextlist $contextlist
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        global $DB;
        $user = $contextlist->get_user();
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        $where = "userid = :userid AND contextid $contextsql";
        $params = [
            'userid' => $user->id,
        ];
        $params += $contextparams;
        $DB->delete_records_select('tool_roleremoval_queue', $where, $params);
    }

    /**
     * Get all users with data in a context.
     *
     * @param \core_privacy\local\request\userlist $userlist
     */
    public static function get_users_in_context(userlist $userlist) {
        $context = $userlist->get_context();
        $sql = "SELECT q.userid
                 FROM {tool_roleremoval_queue} q
                WHERE q.contextid = :context";
        $params = [
            'context' => $context->id,
        ];
        $userlist->add_from_sql('userid', $sql, $params);
    }

    /**
     * Delete data for users in a context.
     *
     * @param \core_privacy\local\request\approved_userlist $userlist
     */
    public static function delete_data_for_users(approved_userlist $userlist) {
        global $DB;
        $context = $userlist->get_context();
        list($usersql, $userparams) = $DB->get_in_or_equal($userlist->get_userids(), SQL_PARAMS_NAMED);

        $where = "userid $usersql AND contextid = :context";
        $params = [
            'context' => $context->id,
        ];
        $params += $userparams;
        $DB->delete_records_select('tool_roleremoval_queue', $where, $params);
    }
}
