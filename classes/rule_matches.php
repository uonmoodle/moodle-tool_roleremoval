<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

/**
 * The amount of matches a rule has can be too much for us to handle in one
 * go so iterate through them in batches to reduce memory use.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rule_matches implements \Iterator {
    /** The number of records we will cache at one time. */
    const LIMIT = 1000;

    /** @var array Cache of results. */
    protected $cache = [];

    /** @var int The record we are looking at. */
    protected $counter = 0;

    /** @var rule */
    protected $rule;

    /**
     * Creates the iterator.
     *
     * @param \tool_roleremoval\rule $rule
     */
    public function __construct(rule $rule) {
        $this->rule = $rule;
    }

    #[\Override]
    public function current(): mixed {
        if (empty($this->cache) && $this->counter === 0) {
            $this->cache_matches(0);
        }

        return $this->cache[$this->counter % self::LIMIT];
    }

    #[\Override]
    public function next(): void {
        $this->counter++;

        if (($this->counter % self::LIMIT) === 0) {
            $this->cache_matches($this->counter);
        }
    }

    #[\Override]
    public function key(): int {
        return $this->counter;
    }

    #[\Override]
    public function valid(): bool {
        if (empty($this->cache) && $this->counter === 0) {
            $this->cache_matches(0);
        }

        return isset($this->cache[$this->counter % self::LIMIT]);
    }

    #[\Override]
    public function rewind(): void {
        $this->counter = 0;
        $this->cache_matches(0);
    }

    /**
     * Fills the cache with matches from the rule.
     *
     * The matches are normally keyed by userid and context id
     * to make the iterator work correctly we need to make sure
     * that we key them with numbers starting at 0.
     *
     * @param int $from
     */
    protected function cache_matches(int $from) {
        $this->cache = array_values($this->rule->get_matches($from, self::LIMIT));
    }
}
