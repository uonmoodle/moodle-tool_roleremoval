<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service for searching contexts.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace tool_roleremoval\external;

use context_system;
use core_external\external_function_parameters;
use core_external\external_multiple_structure;
use core_external\external_single_structure;
use core_external\external_value;
use tool_roleremoval\search;

defined('MOODLE_INTERNAL') || die();

/**
 * Web service for searching contexts.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class search_context extends \core_external\external_api {
    /**
     * Searches for contexts in Moodle that match the query string.
     *
     * @param string $query The query string that should be part of the context name.
     * @param int $limit The maximum results to return.
     * @return array
     */
    public static function get(string $query, int $limit): array {
        // Make sure the user should be here.
        require_capability('tool/roleremoval:edit', context_system::instance());

        // Get the contexts.
        $results = search::context($query, $limit);
        return [
            'contexts' => $results,
        ];
    }

    /**
     * Defines the inputs for the web service method.
     *
     * @return \core_external\external_function_parameters
     */
    public static function get_parameters() {
        return new external_function_parameters(
            [
                'query' => new external_value(PARAM_TEXT, 'The search term', VALUE_REQUIRED),
                'limit' => new external_value(PARAM_INT, 'The search term', VALUE_DEFAULT, 20),
            ]
        );
    }

    /**
     * Defines the output of the web service.
     *
     * @return \core_external\external_function_parameters
     */
    public static function get_returns() {
        return new external_function_parameters(
            [
                'contexts' => new external_multiple_structure(
                    new external_single_structure(
                        [
                            'id' => new external_value(PARAM_INT, 'The id of the context', VALUE_REQUIRED),
                            'name' => new external_value(PARAM_TEXT, 'The name of the context', VALUE_REQUIRED),
                        ]
                    )
                ),
            ]
        );
    }
}
