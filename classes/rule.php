<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use context;
use context_helper;
use context_system;
use dml_missing_record_exception;
use Exception;
use moodle_exception;
use moodle_url;
use tool_roleremoval\output\rules_list;

/**
 * Stores data about role revocation rules.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rule {
    /** @var \context The context of the rule. */
    protected $context;

    /** @var int The id of the context the rule acts on. */
    public $contextid;

    /** @var bool If the rule is enabled for automatic processing. */
    public $enabled;

    /** @var int The database id of the rule. */
    protected $id;

    /** @var int The amount of time a user must be inactive for the rule to trigger on them. */
    public $inactivefor;

    /** @var int The amount of time after a notice has been set before the role is revoked. */
    public $notice;

    /** @var int The id of the role that will be revoked. */
    public $roleid;

    /**
     * A cache of all the roles in Moodle.
     *
     * @var array
     */
    protected static $roles = [];

    /** @var string The name of the rule. */
    public $rulename;

    /**
     * Applies the rule.
     */
    public function apply() {
        // We do this to ensure that all roles will be revoked at the same time.
        $revoketime = time() + $this->notice;

        $rolename = $this->get_rolename();

        while ($users = $this->get_matches_by_user()) {
            foreach ($users as $user) {
                $contexts = [];

                foreach ($user as $match) {
                    // We must record the revocation before we get the context details,
                    // as that will remove them from the record.
                    $this->queue_revocation($match, $revoketime);

                    if (!empty($this->notice)) {
                        // Only get the context details if we will message the user here.
                        // We do not send a message in this loop so that we only send one
                        // message to a user per rule.
                        $ctxid = $match->ctxid;
                        context_helper::preload_from_record($match);
                        $context = context::instance_by_id($ctxid);

                        $contexts[] = [
                            'name' => $context->get_context_name(),
                            'url' => $context->get_url(),
                        ];
                    }
                }

                if (!empty($this->notice) && !empty($user)) {
                    // We will only send a message if the role is not set to be revoked immediately.
                    messenger::notify(current($user), $contexts, $revoketime, $rolename);
                }
            }
        }
    }

    /**
     * Queue a role for removal.
     *
     * @param \stdClass $match A record generated in \tool_roleremoval\rule::get_matches()
     * @param int $revoketime The time the role can be revoked from.
     * @throws \dml_exception
     */
    protected function queue_revocation(\stdClass $match, int $revoketime) {
        global $DB;

        $record = (object) [
            'userid' => $match->id,
            'contextid' => $match->ctxid,
            'roleid' => $this->roleid,
            'revokeon' => $revoketime,
            'created' => time(),
        ];

        $DB->insert_record('tool_roleremoval_queue', $record);
    }

    /**
     * Caches the roles available in Moodle to avoid extra database calls when working with many rules.
     */
    public static function cache_roles() {
        if (empty(static::$roles)) {
            static::$roles = get_all_roles();
        }
    }

    /**
     * Creates a rule from a database record.
     *
     * @param \stdClass $record
     * @return \tool_roleremoval\rule
     */
    public static function create_rule(\stdClass $record): rule {
        $rule = new rule();
        $rule->contextid = (int)$record->contextid;
        $rule->enabled = !empty($record->enabled);
        if (!empty($record->id)) {
            $rule->id = $record->id;
        }
        $rule->inactivefor = (int)$record->inactivefor;
        $rule->notice = (int)$record->notice;
        $rule->roleid = (int)$record->roleid;
        $rule->rulename = $record->rulename;
        return $rule;
    }

    /**
     * Deletes the rule from the database.
     */
    public function delete() {
        global $DB;

        if (empty($this->id)) {
            // The rule is not saved to the database.
            return;
        }

        $DB->delete_records('tool_roleremoval', ['id' => $this->id]);
        $this->id = null;
    }

    /**
     * Tests if a rule is enabled.
     *
     * @return bool
     */
    public function enabled(): bool {
        // Rules that are not valid must not pass as enabled.
        return $this->enabled && $this->valid();
    }

    /**
     * Returns the context that the rule acts upon.
     *
     * An exception will be thrown if the context does not exist.
     *
     * @return \context
     * @throws \coding_exception
     */
    public function get_context(): context {
        if (!isset($this->context)) {
            $this->context = context::instance_by_id($this->contextid);
        }
        return $this->context;
    }

    /**
     * Gets the database id of the rule.
     *
     * @return int|null The id, or null if the rule has not been saved.
     */
    public function get_id(): ?int {
        return $this->id;
    }

    /**
     * Gets a list of users matching the rule.
     *
     * Note: this will return upto 1000 matches at a time.
     *
     * @return array Multi-dimensional array first level is keyed by userid, the second level are the matches for that user.
     */
    public function get_matches_by_user(): array {
        $matches = $this->get_matches(0, 1000);

        $byuser = [];

        // First sort so that we can send one message to each user.
        foreach ($matches as $match) {
            if (!isset($byuser[$match->id])) {
                $byuser[$match->id] = [];
            }
            $byuser[$match->id][$match->ctxid] = $match;
        }

        return $byuser;
    }

    /**
     * Gets users who should be removed by the rule.
     *
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function get_matches($from = 0, $limit = 0): array {
        global $DB;
        $context = $this->get_context();

        $params = [
            'role' => $this->roleid,
            'path' => "$context->path",
            'childpath' => "$context->path/%",
            'pathregexp' => "$context->path($|/.*)",
            'lastaccess' => time() - $this->inactivefor,
            'seperator' => '-',
            'component' => '',
        ];

        if ($DB->sql_regex_supported()) {
            $regexp = $DB->sql_regex();
            $path = "path $regexp :pathregexp";
        } else {
            $childpath = $DB->sql_like('ctx.path', ':childpath');
            $path = "(path = :path OR $childpath)";
        }

        $contextfields = context_helper::get_preload_record_columns_sql('ctx');
        $uid = $DB->sql_concat('u.id', ':seperator', 'ctx.id');

        $sql = "SELECT $uid, u.*, $contextfields
                  FROM {user} u
                  JOIN {role_assignments} ra ON ra.userid = u.id AND component = :component
                  JOIN {context} ctx ON ctx.id = ra.contextid
             LEFT JOIN {tool_roleremoval_queue} q ON q.userid = u.id AND q.contextid = ctx.id
                 WHERE ra.roleid = :role AND $path AND lastaccess < :lastaccess AND q.id IS NULL
              ORDER BY u.id";

        return $DB->get_records_sql($sql, $params, $from, $limit);
    }

    /**
     * Gets the name of the role the rule acts upon.
     *
     * @return string
     */
    public function get_rolename(): string {
        static::cache_roles();

        if (!isset(static::$roles[$this->roleid])) {
            // An invalid role.
            return get_string('invalidrole', 'tool_roleremoval');
        }

        try {
            // Try to get the localised name for the role.
            $name = role_get_name(static::$roles[$this->roleid], $this->get_context(), ROLENAME_BOTH);
        } catch (dml_missing_record_exception $e) {
            // If the context is invalid get the generic name.
            $name = role_get_name(static::$roles[$this->roleid], null, ROLENAME_BOTH);
        }
        return $name;
    }

    /**
     * Loads a rule from the database.
     *
     * @param int $id
     * @return \tool_roleremoval\rule
     */
    public static function get_rule(int $id): rule {
        global $DB;
        $record = $DB->get_record('tool_roleremoval', ['id' => $id], '*', MUST_EXIST);
        return self::create_rule($record);
    }

    /**
     * Gets a list of the rules from the database.
     *
     * @param bool $onlyenabled When true only rules that will be automatically processed are returned.
     * @return \tool_roleremoval\output\rules_list
     * @throws \dml_exception
     */
    public static function get_rules(bool $onlyenabled = false): rules_list {
        global $DB;

        $conditions = null;
        if ($onlyenabled) {
            $conditions = ['enabled' => true];
        }

        $rules = $DB->get_records('tool_roleremoval', $conditions, 'rulename');

        $list = new rules_list();

        foreach ($rules as $record) {
            $rule = static::create_rule($record);
            if ($onlyenabled && !$rule->enabled()) {
                // Something is wrong with the rule, even though it is enabled in the database, so we will ignore it.
                continue;
            }
            $list->rules[$rule->id] = $rule;
        }

        return $list;
    }

    /**
     * Resets static caches in the class.
     *
     * This should only be used in automatic tests.
     *
     * @retrun void
     */
    public static function reset() {
        static::$roles = null;
    }

    /**
     * Saves the rule to the database.
     *
     * @throws \dml_exception
     */
    public function save() {
        global $CFG, $DB;

        $record = (object)(array)$this;

        if (!$this->valid()) {
            // Do not save invalid rules.
            $url = new moodle_url('/' . $CFG->admin . '/tool/roleremoval/index.php');
            throw new moodle_exception('cannotsaveinvalidrule', 'tool_roleremoval', $url, $record);
        }

        if (is_null($this->id)) {
            // Insert a record.
            $this->id = $DB->insert_record('tool_roleremoval', $record);
        } else {
            // Update a record.
            $record->id = $this->id;
            $DB->update_record('tool_roleremoval', $record);
        }
    }

    /**
     * Checks if the rule is valid.
     *
     * @return bool
     */
    public function valid(): bool {
        $valid = true;

        try {
            $this->get_context(); // Throws exception if invalid.
            $valid = $valid && $this->valid_role();
        } catch (Exception $e) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Checks if the role is valid.
     *
     * @return bool
     */
    public function valid_role(): bool {
        $valid = true;
        static::cache_roles();
        if (!isset(static::$roles[$this->roleid])) {
            // The role does not exist.
            $valid = false;
        }
        return $valid;
    }
}
