<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use core\message\message;
use context_system;
use core_user;
use stdClass;

/**
 * Helper class to send messages for the plugin.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class messenger {
    /**
     * Sends a message to the user notifying them that they will have roles revoked unless they login to Moodle.
     *
     * The contexts that will be revoked should be passed in a two-dimensional array:
     *  [
     *      [
     *          'name' => 'The name of the context',
     *          'url' => 'https://url.to/the/context',
     *      ],
     *  ]
     *
     * @param \stdClass $user The user object.
     * @param array $contexts
     * @param int $revoketime The time that the roles will be revoked.
     * @param string $role The name of the role we are revoking.
     * @return void
     */
    public static function notify(stdClass $user, array $contexts, int $revoketime, string $role) {
        global $SITE;

        $contextlist = '';
        foreach ($contexts as $context) {
            $name = $context['name'];
            $url = $context['url'];
            $contextlist .= "* [$name]($url)\n";
        }

        $messageparams = [
            'contextlist' => $contextlist,
            'name' => fullname($user),
            'revocation' => userdate($revoketime, '', $user->timezone),
            'role' => $role,
            'signature' => get_config('tool_roleremoval', 'notifysignature'),
            'since' => userdate($user->lastaccess, '', $user->timezone),
            'sitename' => $SITE->fullname,
        ];

        $text = get_string('message:notify:body', 'tool_roleremoval', $messageparams);

        $message = new message();
        $message->component = 'tool_roleremoval';
        $message->name = 'notify';
        $message->userfrom = core_user::get_noreply_user();
        $message->userto = $user;
        $message->subject = get_string('message:notify:title', 'tool_roleremoval', $messageparams);
        $message->fullmessage = $text;
        $message->fullmessageformat = FORMAT_MARKDOWN;
        $message->fullmessagehtml = format_text($text, FORMAT_MARKDOWN, ['context' => context_system::instance()]);
        $message->notification = 1;

        message_send($message);
    }

    /**
     * Sends a message to the user notifying them that roles in Moodle have been revoked from them.
     *
     * The context and role that will be revoked should be passed in a two-dimensional array:
     *  [
     *      [
     *          'name' => 'The name of the context',
     *          'role' => 'The name of the role',
     *      ],
     *  ]
     *
     * @param \stdClass $user The user object.
     * @param array $contextroles The list of roles and contexts that were revoked from thge user.
     * @return void
     */
    public static function revocation(stdClass $user, array $contextroles) {
        global $SITE;

        $contextlist = '';
        foreach ($contextroles as $context) {
            $name = $context['name'];
            $role = $context['role'];
            $contextlist .= "* $role - $name\n";
        }

        $messageparams = [
            'contextlist' => $contextlist,
            'name' => fullname($user),
            'signature' => get_config('tool_roleremoval', 'revocationsignature'),
            'since' => userdate($user->lastaccess, '', $user->timezone),
            'sitename' => $SITE->fullname,
        ];

        $text = get_string('message:revocation:body', 'tool_roleremoval', $messageparams);

        $message = new message();
        $message->component = 'tool_roleremoval';
        $message->name = 'revocation';
        $message->userfrom = core_user::get_noreply_user();
        $message->userto = $user;
        $message->subject = get_string('message:revocation:title', 'tool_roleremoval', $messageparams);
        $message->fullmessage = $text;
        $message->fullmessageformat = FORMAT_MARKDOWN;
        $message->fullmessagehtml = format_text($text, FORMAT_MARKDOWN, ['context' => context_system::instance()]);
        $message->notification = 1;

        message_send($message);
    }
}
