<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page used to export users who match a rule.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core\dataformat;
use tool_roleremoval\rule;
use tool_roleremoval\rule_matches;
use tool_roleremoval\export;
use core_user\fields;

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir . '/dataformatlib.php');

$id = required_param('id', PARAM_INT);
$paramname = 'dataformat';
$dataformat = optional_param($paramname, '', PARAM_ALPHA);

$pageparams = [
    'id' => $id,
];

$pageurl = '/' . $CFG->admin . '/tool/roleremoval/exportmatches.php';

admin_externalpage_setup('toolroleremoval', '', $pageparams, $pageurl);

$rule = rule::get_rule($id);

if (!empty($dataformat)) {
    // Do the export.
    $columns = [
        'fullname' => get_string('fullname'),
    ];

    $extrafields = fields::for_identity(context_system::instance())
        ->excluding('lastaccess')
        ->get_required_fields();
    foreach ($extrafields as $field) {
        $columns[$field] = fields::get_display_name($field);
    }

    $columns['lastaccess'] = fields::get_display_name('lastaccess');
    $columns['contextname'] = get_string('context', 'tool_roleremoval');
    $columns['contexturl'] = get_string('contexturl', 'tool_roleremoval');

    $callback = [export::class, 'format_match_record'];
    $roles = get_all_roles();
    $filename = 'tool_roleremoval_export_matches_' . $rule->get_id() . '_' . $roles[$rule->roleid]->shortname . '_' . time();
    $matches = new rule_matches($rule);
    dataformat::download_data($filename, $dataformat, $columns, $matches, $callback);
    exit;
}

// Let the user select the export format.
/* @global \core_renderer $OUTPUT The global renderer. */
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('exportlistprompt', 'tool_roleremoval', $rule->rulename));
echo $OUTPUT->download_dataformat_selector(
    get_string('selectformat', 'tool_roleremoval'),
    new moodle_url($pageurl),
    $paramname,
    $pageparams
);
echo $OUTPUT->footer();
