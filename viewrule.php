<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The page for viewing who the rule will match.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use tool_roleremoval\rule;
use tool_roleremoval\output\user_list;

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->libdir.'/adminlib.php');

$id = required_param('id', PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);

$pageparams = [
    'id' => $id,
];

$pageurl = '/' . $CFG->admin . '/tool/roleremoval/viewrule.php';

admin_externalpage_setup('toolroleremoval', '', $pageparams, $pageurl);

$limit = 50;
$start = $limit * ($page - 1);

$userlist = new user_list();
$userlist->rule = rule::get_rule($id);
$userlist->page = $page;

/* @global \core_renderer $OUTPUT The global renderer. */
echo $OUTPUT->header();
echo $OUTPUT->render($userlist);
echo $OUTPUT->footer();
