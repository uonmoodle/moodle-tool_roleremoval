<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page used to preview how messages sent will look.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->libdir.'/adminlib.php');

$pageurl = '/' . $CFG->admin . '/tool/roleremoval/preview_messages.php';

admin_externalpage_setup('toolroleremovaltestmessages', '', null, $pageurl);

/* @global \core_renderer $OUTPUT The global renderer. */
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('testmessages', 'tool_roleremoval'));

/* @global \stdClass $SITE The details about the Moodle site. */
$contextlist = '* [Some course](http://example.com)
* [Some cagegory](http://example.com)';
$exampledata = [
    'contextlist' => $contextlist,
    'name' => 'Example User',
    'revocation' => userdate(time() - WEEKSECS),
    'role' => 'Teacher',
    'signature' => get_config('tool_roleremoval', 'notifysignature'),
    'since' => userdate(time() - YEARSECS),
    'sitename' => $SITE->fullname,
];
echo $OUTPUT->heading(get_string('message:notify:title', 'tool_roleremoval', $exampledata), 3);
echo $OUTPUT->box(
    format_text(
        get_string('message:notify:body', 'tool_roleremoval', $exampledata),
        FORMAT_MARKDOWN,
        ['context' => context_system::instance()]
    )
);

$contextlist = '* Teacher - Some course
* Manager - Some category';
$exampledata = [
    'contextlist' => $contextlist,
    'name' => 'Example User',
    'revocation' => userdate(time() - WEEKSECS),
    'role' => 'Teacher',
    'signature' => get_config('tool_roleremoval', 'revocationsignature'),
    'since' => userdate(time() - YEARSECS),
    'sitename' => $SITE->fullname,
];
echo $OUTPUT->heading(get_string('message:revocation:title', 'tool_roleremoval', $exampledata), 3);
echo $OUTPUT->box(
    format_text(
        get_string('message:revocation:body', 'tool_roleremoval', $exampledata),
        FORMAT_MARKDOWN,
        ['context' => context_system::instance()]
    )
);

echo $OUTPUT->footer();
