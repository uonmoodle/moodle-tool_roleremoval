<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main page for the role revocation tool.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use tool_roleremoval\rule;

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->libdir.'/adminlib.php');

admin_externalpage_setup('toolroleremoval');

$rulelist = rule::get_rules();

/* @global \core_renderer $OUTPUT The global renderer. */
echo $OUTPUT->header();
echo $OUTPUT->render($rulelist);
echo $OUTPUT->footer();
