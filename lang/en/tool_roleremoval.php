<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for the role revocation tool
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['activatesafter'] = 'Users must be inactive for {$a}';
$string['cannotsaveinvalidrule'] = 'Cannot save invalid rules';
$string['context'] = 'Context';
$string['contextid'] = 'Context';
$string['contextid_help'] = 'The context that will work in (all sub contexts will also have the rule applied to them)';
$string['contexturl'] = 'Context url';
$string['deleterule'] = 'Are you sure you with to delete "{$a}" rule?';
$string['duplicaterule'] = 'The context and role duplicates an existing rule.';
$string['editrule'] = 'Edit rule';
$string['exportlistprompt'] = 'Export matches for {$a} rule';
$string['exportmatches'] = 'Download match information';
$string['inactivefor'] = 'Trigger time';
$string['inactivefor_help'] = 'The amount of time a user must be inactive for the rule to trigger on them';
$string['invalidcontext'] = 'The context does not exist';
$string['invalidrole'] = 'The role does not exist';
$string['messageprovider:notify'] = 'Warning that you will have a role revoked';
$string['messageprovider:revocation'] = 'Confirmation that you have had a role revoked';
$string['messagesettings'] = 'Message settings';
$string['messagesettings_help'] = 'Preview how messages will look based on the settings: {$a}';
$string['message:notify:title'] = 'Log in to {$a->sitename} to keep your roles';
$string['message:notify:body'] = 'Dear {$a->name},

You have not logged into {$a->sitename} since {$a->since}, in order to maintain data security we may revoke your {$a->role} role in
the following places:

{$a->contextlist}
 
If you wish to preserve this access please log into {$a->sitename} before {$a->revocation}.

{$a->signature}';
$string['message:revocation:title'] = 'Roles revoked on {$a->sitename} due to inactivity';
$string['message:revocation:body'] = 'Dear {$a->name},

You have not logged into {$a->sitename} since {$a->since} so in order to maintain data security the following roles have been
revoked:

{$a->contextlist}

{$a->signature}';
$string['newrule'] = 'Create rule';
$string['norules'] = 'No rules have been created';
$string['notice'] = 'Notice period';
$string['notice_help'] = 'The amount of time a user will be given before their role is revoked.

If 0 is provided their role will be revoked without them being given advanced notification';
$string['noticeperiod'] = 'Users given {$a} to login';
$string['notifysignature'] = 'Notification message signature';
$string['notifysignature_help'] = 'This text will be displayed at the bottom of the message sent
to users when they are about to have roles revoked. You can use markdown to format the text.';
$string['pluginname'] = 'Role revocation';
$string['privacy:export:queueitem'] = 'Role revocation instruction';
$string['privacy:metadata:core_message'] = 'Messages are sent to users when a user meets a rule for role 
revocation, and when their role is revoked';
$string['privacy:metadata:tool_roleremoval_queue'] = 'A queue of instructions to revoke roles from users';
$string['privacy:metadata:tool_roleremoval_queue:contextid'] = 'The context the role will be revoked from';
$string['privacy:metadata:tool_roleremoval_queue:created'] = 'The time that the revocation instruction was created';
$string['privacy:metadata:tool_roleremoval_queue:revokeon'] = 'The time after which the role can be revoked';
$string['privacy:metadata:tool_roleremoval_queue:roleid'] = 'The role that will be revoked';
$string['privacy:metadata:tool_roleremoval_queue:userid'] = 'The id of the user who will have a role revoked';
$string['revocationsignature'] = 'Revocation message signature';
$string['revocationsignature_help'] = 'This text will be displayed at the bottom of the message sent
to users when they have had roles revoked. You can use markdown to format the text.';
$string['roleid'] = 'Role';
$string['roleid_help'] = 'The role that will be removed by the rule';
$string['roleremoval:edit'] = 'Can create and edit rules';
$string['roleremoval:run'] = 'Can manually trigger rules';
$string['ruledeleted'] = 'Rule "{$a}" was deleted';
$string['ruledisabled'] = 'Manual rule';
$string['ruleenabled'] = 'Automatic rule';
$string['ruleenabled_help'] = 'If enabled the rule will be automatically processed by a scheduled task while it is still valid';
$string['ruleinvalid'] = 'Invalid rule';
$string['rulelist'] = 'Role revocation rule list';
$string['rulename'] = 'Name';
$string['rulename_help'] = 'A descriptive name for the rule';
$string['ruletriggered'] = 'The rule has been queued to run';
$string['rulesaved'] = 'Rule saved';
$string['rulesummary'] = '{$a->role} will be revoked {$a->notice} after the rule is run.';
$string['runrule'] = 'Run rule';
$string['selectformat'] = 'Select export format';
$string['testmessages'] = 'Preview messages';
$string['task:applyrules'] = 'Run automatic rules';
$string['task:applyrule'] = 'Run a manual rule';
$string['task:revokeroles'] = 'Role revocation';
$string['task_apply_rule_duplicate'] = 'You may not run the rule while waiting for a previous run to complete.';
$string['triggerrule'] = 'Running the "{$a->rulename}" rule will result in all users who have not logged in for {$a->inactivefor} to 
have the {$a->role} role revoked from {$a->context} in {$a->notice} unless they login before then.';
$string['usersmatchingrule'] = 'Sample of matches for the rule';
$string['viewmatches'] = 'View matching users';
