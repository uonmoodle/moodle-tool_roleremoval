<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The page for editing rules.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core\output\notification;
use tool_roleremoval\rule;
use tool_roleremoval\rule_form;

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->libdir.'/adminlib.php');

$id = optional_param('id', 0, PARAM_INT);

$pageparams = [];

if (!empty($id)) {
    $pageparams['id'] = $id;
}

$pageurl = '/' . $CFG->admin . '/tool/roleremoval/editrule.php';

admin_externalpage_setup('toolroleremoval', '', $pageparams, $pageurl);

require_capability('tool/roleremoval:edit', context_system::instance());

$form = new rule_form();

$returnurl = new moodle_url('/' . $CFG->admin . '/tool/roleremoval/index.php');

if ($form->is_cancelled()) {
    // Redirect to the rules index page.
    redirect($returnurl);
} else if ($form->is_submitted() && $form->is_validated()) {
    try {
        $form->save();
        redirect($returnurl, get_string('rulesaved', 'tool_roleremoval'), 0, notification::NOTIFY_SUCCESS);
    } catch (dml_write_exception $e) {
        \core\notification::add(get_string('duplicaterule', 'tool_roleremoval'), notification::NOTIFY_ERROR);
    }
}

if (!empty($id)) {
    // We are editing an existing rule.
    $rule = rule::get_rule($id);
    $data = (object)(array) $rule;
    $data->id = $rule->get_id();
    $form->set_data($data);
}

/* @global \core_renderer $OUTPUT The global renderer. */
echo $OUTPUT->header();
$form->display();
echo $OUTPUT->footer();
