<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The page for deleting rules.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core\output\notification;
use tool_roleremoval\rule;

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->libdir . '/adminlib.php');

$id = required_param('id', PARAM_INT);
$confirm = optional_param('confirm', false, PARAM_BOOL);

$pageparams = [
    'id' => $id,
];
$pageurl = '/' . $CFG->admin . '/tool/roleremoval/deleterule.php';

admin_externalpage_setup('toolroleremoval', '', $pageparams, $pageurl);

require_capability('tool/roleremoval:edit', context_system::instance());

$rule = rule::get_rule($id);

$returnurl = new moodle_url('/' . $CFG->admin . '/tool/roleremoval/index.php');

if ($confirm) {
    require_sesskey();
    $rule->delete();
    $message = get_string('ruledeleted', 'tool_roleremoval', format_string($rule->rulename));
    redirect($returnurl, $message, 0, notification::NOTIFY_SUCCESS);
}

/* @global \core_renderer $OUTPUT The global renderer. */
echo $OUTPUT->header();
$message = get_string('deleterule', 'tool_roleremoval', format_string($rule->rulename));
$confirmurl = new moodle_url($pageurl, ['id' => $id, 'confirm' => 1]);
echo $OUTPUT->confirm($message, $confirmurl, $returnurl);
echo $OUTPUT->footer();
