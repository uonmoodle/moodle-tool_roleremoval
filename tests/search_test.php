<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use context_course;
use context_coursecat;
use context_system;

/**
 * Tests the \tool_roleremoval\search class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\search
 */
class search_test extends \advanced_testcase {
    /**
     * Tests that we can search contexts correctly.
     */
    public function test_context() {
        $this->resetAfterTest(true);

        $generator = $this->getDataGenerator();

        $category1 = $generator->create_category(['name' => 'Red']);
        $category2 = $generator->create_category(['name' => 'Blue']);
        $category3 = $generator->create_category(['name' => 'Green']);

        $course1 = $generator->create_course(['fullname' => 'Red']);
        $course2 = $generator->create_course(['fullname' => 'Blue', 'category' => $category1->id]);
        $generator->create_course(['fullname' => 'Green', 'category' => $category3->id]);
        $generator->create_course(['fullname' => 'Yellow']);
        $course5 = $generator->create_course(['fullname' => 'Blurple', 'category' => $category2->id]);

        $syscontext = context_system::instance();

        $catcontext1 = context_coursecat::instance($category1->id);
        $catcontext2 = context_coursecat::instance($category2->id);
        $catcontext3 = context_coursecat::instance($category3->id);

        $coursecontext1 = context_course::instance($course1->id);
        $coursecontext2 = context_course::instance($course2->id);
        $coursecontext5 = context_course::instance($course5->id);

        // Searching with an empty string should return only the system context.
        $expected1 = [
            (object) [
                'id' => $syscontext->id,
                'name' => $syscontext->get_context_name(),
            ],
        ];
        $this->assertEquals($expected1, search::context(''));

        // Search with a full match.
        $expected2 = [
            (object) [
                'id' => $catcontext1->id,
                'name' => $catcontext1->get_context_name(),
            ],
            (object) [
                'id' => $coursecontext1->id,
                'name' => $coursecontext1->get_context_name(),
            ],
        ];
        $this->assertEquals($expected2, search::context('Red'));

        // Search with a partial match.
        $expected3 = [
            (object) [
                'id' => $catcontext2->id,
                'name' => $catcontext2->get_context_name(),
            ],
            (object) [
                'id' => $coursecontext2->id,
                'name' => $coursecontext2->get_context_name(),
            ],
            (object) [
                'id' => $coursecontext5->id,
                'name' => $coursecontext5->get_context_name(),
            ],
        ];
        $this->assertEquals($expected3, search::context('lu'));

        // Limit the results.
        $expected3 = [
            (object) [
                'id' => $catcontext3->id,
                'name' => $catcontext3->get_context_name(),
            ],
        ];
        $this->assertEquals($expected3, search::context('Green', 1));
    }
}
