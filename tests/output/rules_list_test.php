<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\output;

use context_coursecat;
use tool_roleremoval\rule;

/**
 * Tests the \tool_roleremoval\output\rules_list class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\output\rules_list
 */
class rules_list_test extends \advanced_testcase {
    /**
     * Test that the renderable can handle invalid rules correctly.
     */
    public function test_export_for_template() {
        global $OUTPUT;

        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $cat2 = $generator->create_category([]);
        $deletedcontext = context_coursecat::instance($cat2->id);
        $deletedcontext->delete();
        $role = $generator->create_role(['shortname' => 'testingrole']);

        // Find the highest database id for a role.
        $roleids = array_keys(get_all_roles());
        asort($roleids, SORT_NUMERIC);
        $maxroleid = array_pop($roleids);

        // Valid rule.
        $data1 = (object) [
            'id' => 73,
            'contextid' => $catcontext->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $role,
            'rulename' => 'Testing rule',
        ];
        $rule1 = rule::create_rule($data1);

        // Invalid context.
        $data2 = (object) [
            'id' => 73,
            'contextid' => $deletedcontext->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $role,
            'rulename' => 'Testing rule',
        ];
        $rule2 = rule::create_rule($data2);

        // Valid role.
        $data3 = (object) [
            'id' => 73,
            'contextid' => $catcontext->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $maxroleid + 1,
            'rulename' => 'Testing rule',
        ];
        $rule3 = rule::create_rule($data3);

        $list = new rules_list();
        $list->rules = [$rule1, $rule2, $rule3];

        $this->setAdminUser();
        $this->assertNotEmpty($OUTPUT->render($list));
    }
}
