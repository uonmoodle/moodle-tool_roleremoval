<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use context_course;
use context_coursecat;
use dml_missing_record_exception;
use moodle_exception;
use tool_roleremoval\output\rules_list;

/**
 * Tests the \tool_roleremoval\rule class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\rule
 */
class rule_test extends \advanced_testcase {
    /**
     * @inheritDoc
     */
    public function tearDown(): void {
        parent::tearDown();
        rule::reset();
    }

    /**
     * Tests that rules are applied properly.
     */
    public function test_apply() {
        global $DB;

        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('notifysignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        $course1 = $generator->create_course(['category' => $cat->id]);
        $course1context = context_course::instance($course1->id);
        $course2 = $generator->create_course(['category' => $cat->id]);
        $course2context = context_course::instance($course1->id);

        $activetime = time();
        $inactivetime = time() - DAYSECS;

        // Users in the right area that have not logged in recently.
        $user1 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user1->id, $course1->id, 'testingrole');
        $generator->enrol_user($user1->id, $course2->id, 'testingrole');
        $user2 = $generator->create_user(['lastaccess' => $activetime]);
        $generator->enrol_user($user2->id, $course1->id, 'testingrole');

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(['contextid' => $catcontext->id, 'roleid' => $role, 'inactivefor' => HOURSECS]);

        $rule = rule::get_rule($rulerecord->id);

        $queuecount = $DB->count_records('tool_roleremoval_queue');

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $rule->apply();

        // Test that the queue was filled correctly.
        $this->assertEquals($queuecount + 2, $DB->count_records('tool_roleremoval_queue'));
        $this->assertTrue(
            $DB->record_exists(
                'tool_roleremoval_queue',
                [
                    'userid' => $user1->id,
                    'roleid' => $role,
                    'contextid' => $course1context->id
                ]
            )
        );
        $this->assertTrue(
            $DB->record_exists(
                'tool_roleremoval_queue',
                [
                    'userid' => $user1->id,
                    'roleid' => $role,
                    'contextid' => $course2context->id
                ]
            )
        );

        // Test messages were sent.
        $this->assertEquals(1, $sink->count());
        $messages = $sink->get_messages();
        $message = current($messages);
        $this->assertEquals($user1->id, $message->useridto);
        $this->assertEquals('tool_roleremoval', $message->component);
        $this->assertEquals('notify', $message->eventtype);
    }

    /**
     * Tests that rules are applied properly when a rule has no notice period.
     */
    public function test_apply_no_notice() {
        global $DB;

        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('notifysignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        $course1 = $generator->create_course(['category' => $cat->id]);
        $course1context = context_course::instance($course1->id);
        $course2 = $generator->create_course(['category' => $cat->id]);
        $course2context = context_course::instance($course1->id);

        $activetime = time();
        $inactivetime = time() - DAYSECS;

        // Users in the right area that have not logged in recently.
        $user1 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user1->id, $course1->id, 'testingrole');
        $generator->enrol_user($user1->id, $course2->id, 'testingrole');
        $user2 = $generator->create_user(['lastaccess' => $activetime]);
        $generator->enrol_user($user2->id, $course1->id, 'testingrole');

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(
            [
                'contextid' => $catcontext->id,
                'roleid' => $role,
                'inactivefor' => HOURSECS,
                'notice' => 0
            ]
        );

        $rule = rule::get_rule($rulerecord->id);

        $queuecount = $DB->count_records('tool_roleremoval_queue');

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $rule->apply();

        // Test that the queue was filled correctly.
        $this->assertEquals($queuecount + 2, $DB->count_records('tool_roleremoval_queue'));
        $this->assertTrue(
            $DB->record_exists(
                'tool_roleremoval_queue',
                [
                    'userid' => $user1->id,
                    'roleid' => $role,
                    'contextid' => $course1context->id
                ]
            )
        );
        $this->assertTrue(
            $DB->record_exists(
                'tool_roleremoval_queue',
                [
                    'userid' => $user1->id,
                    'roleid' => $role,
                    'contextid' => $course2context->id
                ]
            )
        );

        // Test no messages were sent.
        $this->assertEquals(0, $sink->count());
    }

    /**
     * Test creating a rule from a record.
     */
    public function test_create_rule() {
        // Tests creating a rule using no id.
        $data1 = (object) [
            'contextid' => 1234567,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => 67,
            'rulename' => 'Testing rule',
        ];
        $rule1 = rule::create_rule($data1);

        $this->assertInstanceOf(rule::class, $rule1);
        $this->assertEquals($data1->contextid, $rule1->contextid);
        $this->assertEquals($data1->enabled, $rule1->enabled);
        $this->assertEquals($data1->inactivefor, $rule1->inactivefor);
        $this->assertEquals($data1->notice, $rule1->notice);
        $this->assertEquals($data1->roleid, $rule1->roleid);
        $this->assertEquals($data1->rulename, $rule1->rulename);
        $this->assertNull($rule1->get_id());

        // Tests creating a rule using an id.
        $data2 = (object) [
            'id' => 73,
            'contextid' => 1234567,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => 67,
            'rulename' => 'Testing rule',
        ];
        $rule2 = rule::create_rule($data2);

        $this->assertInstanceOf(rule::class, $rule2);
        $this->assertEquals($data2->contextid, $rule2->contextid);
        $this->assertEquals($data2->enabled, $rule2->enabled);
        $this->assertEquals($data2->inactivefor, $rule2->inactivefor);
        $this->assertEquals($data2->notice, $rule2->notice);
        $this->assertEquals($data2->roleid, $rule2->roleid);
        $this->assertEquals($data2->rulename, $rule2->rulename);
        $this->assertEquals($data2->id, $rule2->get_id());
    }

    /**
     * Tests that rules are deleted from the database.
     */
    public function test_delete() {
        global $DB;

        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $othercat = $generator->create_category([]);
        $role = $generator->create_role([]);
        $otherrole = $generator->create_role([]);
        $context = context_coursecat::instance($cat->id);
        $othercontext = context_coursecat::instance($othercat->id);
        $rulerecord = $generator->create_rule(['contextid' => $context->id, 'roleid' => $role]);
        // Create some other rules to make sure they are not removed.
        $generator->create_rule(['contextid' => $othercontext->id, 'roleid' => $role]);
        $generator->create_rule(['contextid' => $context->id, 'roleid' => $otherrole]);

        $startingrules = $DB->count_records('tool_roleremoval');

        $rule = rule::get_rule($rulerecord->id);
        $rule->delete();

        $this->assertEquals($startingrules - 1, $DB->count_records('tool_roleremoval'));
        $this->assertFalse($DB->record_exists('tool_roleremoval', ['contextid' => $context->id, 'roleid' => $role]));
    }

    /**
     * Tests that rules are detected correctly as being eligible to run automatically.
     *
     * @param bool $enabled The run automatically setting.
     * @param bool $validrole Create a valid rule.
     * @param bool $expected The expects value.
     * @dataProvider data_enabled
     */
    public function test_enabled(bool $enabled, bool $validrule, bool $expected) {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $role = $generator->create_role([]);
        $context = context_coursecat::instance($cat->id);
        $rulerecord = $generator->create_rule(['contextid' => $context->id, 'roleid' => $role, 'enabled' => $enabled]);

        if (!$validrule) {
            // Now that we have created the rule delete its context so that it is invalid.
            $context->delete();
        }

        $rule = rule::get_rule($rulerecord->id);
        $this->assertEquals($expected, $rule->enabled());
    }

    /**
     * Data provider for test_enabled.
     *
     * @return array
     */
    public function data_enabled() : array {
        return [
            'valid automatic' => [true, true, true],
            'valid manual' => [false, true, false],
            'invalid automatic' => [true, false, false],
            'invalid manual' => [false, false, false],
        ];
    }

    /**
     * Tests that we can get the context for a rule.
     */
    public function test_get_context() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $role = $generator->create_role([]);
        $context = context_coursecat::instance($cat->id);
        $rulerecord = $generator->create_rule(['contextid' => $context->id, 'roleid' => $role]);

        $rule = rule::get_rule($rulerecord->id);
        $this->assertEquals($context, $rule->get_context());
    }

    /**
     * Tests that trying to fetch an invalid context.
     */
    public function test_get_context_invalid() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $role = $generator->create_role([]);
        $context = context_coursecat::instance($cat->id);
        $rulerecord = $generator->create_rule(['contextid' => $context->id, 'roleid' => $role]);

        $context->delete();

        $rule = rule::get_rule($rulerecord->id);
        $this->expectException(dml_missing_record_exception::class);
        $rule->get_context();
    }

    /**
     * Tests that we can get the id of a rule.
     */
    public function test_get_id() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $role = $generator->create_role([]);
        $context = context_coursecat::instance($cat->id);

        $data = (object) [
            'contextid' => $context->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $role,
            'rulename' => 'Testing rule',
        ];

        $rule = rule::create_rule($data);

        // Before a rule is saved there should be no id.
        $this->assertNull($rule->get_id());

        $rule->save();

        // After a rule is saved there should be an it number.
        $this->assertNotNull($rule->get_id());
        $this->assertGreaterThan(0, $rule->get_id());
    }

    /**
     * Tests that we can get the users matches easily.
     */
    public function test_get_matches_by_user() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        $course1 = $generator->create_course(['category' => $cat->id]);
        $course1context = context_course::instance($course1->id);
        $course2 = $generator->create_course(['category' => $cat->id]);
        $course2context = context_course::instance($course1->id);

        $inactivetime = time() - DAYSECS;

        // Users in the right area that have not logged in recently.
        $user1 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user1->id, $course1->id, 'testingrole');
        $generator->enrol_user($user1->id, $course2->id, 'testingrole');
        $user2 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user2->id, $course1->id, 'testingrole');
        $user3 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user3->id, $catcontext->id);

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(['contextid' => $catcontext->id, 'roleid' => $role, 'inactivefor' => HOURSECS]);

        $rule = rule::get_rule($rulerecord->id);

        $matches = $rule->get_matches_by_user();

        $this->assertCount(3, $matches);
        $this->assertTrue(isset($matches[$user1->id]));
        $this->assertCount(2, $matches[$user1->id]);
        $this->assertTrue(isset($matches[$user1->id][$course1context->id]));
        $this->assertTrue(isset($matches[$user1->id][$course2context->id]));
        $this->assertTrue(isset($matches[$user2->id]));
        $this->assertCount(1, $matches[$user2->id]);
        $this->assertTrue(isset($matches[$user2->id][$course1context->id]));
        $this->assertTrue(isset($matches[$user3->id]));
        $this->assertCount(1, $matches[$user3->id]);
        $this->assertTrue(isset($matches[$user3->id][$catcontext->id]));
    }

    /**
     * Tests that we can get matched.
     */
    public function test_get_matches() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $othercat = $generator->create_category([]);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        $course1 = $generator->create_course(['category' => $cat->id]);
        $course1context = context_course::instance($course1->id);
        $course2 = $generator->create_course(['category' => $cat->id]);
        $course2context = context_course::instance($course1->id);
        $othercourse = $generator->create_course(['category' => $othercat->id]);

        $activetime = time();
        $inactivetime = time() - DAYSECS;

        // Users in the right area that have not logged in recently.
        $user1 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user1->id, $course1->id, 'testingrole');
        $generator->enrol_user($user1->id, $course2->id, 'testingrole');
        $user2 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user2->id, $course1->id, 'testingrole');
        $user3 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user3->id, $catcontext->id);

        // Recently logged in.
        $user4 = $generator->create_user(['lastaccess' => $activetime]);
        $generator->enrol_user($user4->id, $course1->id, 'testingrole');

        // Wrong role.
        $user5 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user5->id, $course2->id, 'student');
        $generator->enrol_user($user3->id, $course2->id, 'student');

        // Wrong context path.
        $user6 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->enrol_user($user6->id, $othercourse->id, 'testingrole');

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(['contextid' => $catcontext->id, 'roleid' => $role, 'inactivefor' => HOURSECS]);

        $rule = rule::get_rule($rulerecord->id);

        $matches = $rule->get_matches();

        $this->assertCount(4, $matches);
        $this->assertTrue(isset($matches["{$user1->id}-{$course1context->id}"]));
        $this->assertTrue(isset($matches["{$user1->id}-{$course2context->id}"]));
        $this->assertTrue(isset($matches["{$user2->id}-{$course1context->id}"]));
        $this->assertTrue(isset($matches["{$user3->id}-{$catcontext->id}"]));

        // Test that paging the results works.
        $limitedmatches = $rule->get_matches(2, 1);
        $this->assertCount(1, $limitedmatches);
        $this->assertTrue(isset($limitedmatches["{$user2->id}-{$course1context->id}"]));
    }

    /**
     * Tests that we can get a rule from the database.
     */
    public function test_get_rule() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $role = $generator->create_role([]);
        $context = context_coursecat::instance($cat->id);
        $rulerecord = $generator->create_rule(['contextid' => $context->id, 'roleid' => $role]);

        $this->assertEquals(rule::create_rule($rulerecord), rule::get_rule($rulerecord->id));
    }

    /**
     * Tests that there is an exception if we try to get a rule that has no record.
     */
    public function test_get_rule_invalid() {
        $this->expectException(dml_missing_record_exception::class);
        rule::get_rule(42);
    }

    /**
     * Tests that we can get sets of rules properly.
     */
    public function test_get_rules() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $role = $generator->create_role([]);
        $cat1 = $generator->create_category([]);
        $cat2 = $generator->create_category([]);
        $cat3 = $generator->create_category([]);

        $context1 = context_coursecat::instance($cat1->id);
        $context2 = context_coursecat::instance($cat2->id);
        $context3 = context_coursecat::instance($cat3->id);

        $automaticrecord = $generator->create_rule(['contextid' => $context1->id, 'roleid' => $role, 'enabled' => true]);
        $manualrecord = $generator->create_rule(['contextid' => $context2->id, 'roleid' => $role, 'enabled' => false]);
        $invalidrecord = $generator->create_rule(['contextid' => $context3->id, 'roleid' => $role, 'enabled' => true]);

        $context3->delete();

        $allrules = rule::get_rules();
        $this->assertInstanceOf(rules_list::class, $allrules);
        $this->assertCount(3, $allrules->rules);
        $this->assertTrue(isset($allrules->rules[$automaticrecord->id]));
        $this->assertInstanceOf(rule::class, $allrules->rules[$automaticrecord->id]);
        $this->assertTrue(isset($allrules->rules[$manualrecord->id]));
        $this->assertInstanceOf(rule::class, $allrules->rules[$manualrecord->id]);
        $this->assertTrue(isset($allrules->rules[$invalidrecord->id]));
        $this->assertInstanceOf(rule::class, $allrules->rules[$invalidrecord->id]);

        $enabledrules = rule::get_rules(true);
        $this->assertInstanceOf(rules_list::class, $enabledrules);
        $this->assertCount(1, $enabledrules->rules);
        $this->assertTrue(isset($enabledrules->rules[$automaticrecord->id]));
        $this->assertInstanceOf(rule::class, $enabledrules->rules[$automaticrecord->id]);
    }

    /**
     * Tests that a rule will save correctly.
     */
    public function test_save() {
        global $DB;

        $this->resetAfterTest(true);

        $cat = $this->getDataGenerator()->create_category([]);
        $role = $this->getDataGenerator()->create_role([]);
        $context = context_coursecat::instance($cat->id);

        $data = (object) [
            'contextid' => $context->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $role,
            'rulename' => 'Testing rule',
        ];

        $startingrules = $DB->count_records('tool_roleremoval');

        // Test that new rules are created.
        $rule = rule::create_rule($data);
        $rule->save();

        $this->assertEquals($startingrules + 1, $DB->count_records('tool_roleremoval'));
        $this->assertTrue($DB->record_exists('tool_roleremoval', (array) $data));

        // Test that saving again updates a record, rather than creating a new one.
        $data->rulename = 'Modified rule name';
        $rule->rulename = $data->rulename;
        $rule->save();

        $this->assertEquals($startingrules + 1, $DB->count_records('tool_roleremoval'));
        $this->assertTrue($DB->record_exists('tool_roleremoval', (array) $data));
    }

    /**
     * Tests that a rule cannot be saved for an invalid context.
     */
    public function test_save_invalid_context() {
        $this->resetAfterTest(true);

        $cat = $this->getDataGenerator()->create_category([]);
        $role = $this->getDataGenerator()->create_role([]);
        $context = context_coursecat::instance($cat->id);

        $data = (object) [
            'contextid' => $context->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $role,
            'rulename' => 'Testing rule',
        ];

        // Delete the context, we created one to absolutely ensure we were not using a real context.
        $context->delete();

        $rule = rule::create_rule($data);

        // Test that the save fails.
        $this->expectException(moodle_exception::class);
        $this->expectExceptionMessage(get_string('cannotsaveinvalidrule', 'tool_roleremoval'));
        $rule->save();
    }

    /**
     * Tests that a rule cannot be saved for an invalid role.
     */
    public function test_save_invalid_role() {
        $this->resetAfterTest(true);

        $cat = $this->getDataGenerator()->create_category([]);
        $context = context_coursecat::instance($cat->id);

        // Find the highest database id for a role.
        $roleids = array_keys(get_all_roles());
        asort($roleids, SORT_NUMERIC);
        $maxroleid = array_pop($roleids);

        $data = (object) [
            'contextid' => $context->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $maxroleid + 1,
            'rulename' => 'Testing rule',
        ];

        $rule = rule::create_rule($data);

        // Test that the save fails.
        $this->expectException(moodle_exception::class);
        $this->expectExceptionMessage(get_string('cannotsaveinvalidrule', 'tool_roleremoval'));
        $rule->save();
    }

    /**
     * Tests that a rule validity is properly detected.
     */
    public function test_valid() {
        $this->resetAfterTest(true);

        $cat = $this->getDataGenerator()->create_category([]);
        $role = $this->getDataGenerator()->create_role([]);
        $context = context_coursecat::instance($cat->id);

        // Get an invalid context.
        $othercat = $this->getDataGenerator()->create_category([]);
        $othercontext = context_coursecat::instance($othercat->id);
        $othercontext->delete();

        // Find the highest database id for a role.
        $roleids = array_keys(get_all_roles());
        asort($roleids, SORT_NUMERIC);
        $maxroleid = array_pop($roleids);

        $data = (object) [
            'contextid' => $context->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $role,
            'rulename' => 'Testing rule',
        ];

        $rule = rule::create_rule($data);
        $this->assertTrue($rule->valid());

        // Test that invalid roles are rejected.
        $data->roleid = $maxroleid + 1;
        $rule = rule::create_rule($data);
        $this->assertFalse($rule->valid());

        // Test invalid contexts are rejected.
        $data->roleid = $role;
        $data->contextid = $othercontext->id;
        $rule = rule::create_rule($data);
        $this->assertFalse($rule->valid());
    }

    /**
     * Tests that invalid roles are detected correctly.
     */
    public function test_valid_role() {
        $this->resetAfterTest(true);

        $cat = $this->getDataGenerator()->create_category([]);
        $role = $this->getDataGenerator()->create_role([]);
        $context = context_coursecat::instance($cat->id);

        // Find the highest database id for a role.
        $roleids = array_keys(get_all_roles());
        asort($roleids, SORT_NUMERIC);
        $maxroleid = array_pop($roleids);

        $data = (object) [
            'contextid' => $context->id,
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'roleid' => $role,
            'rulename' => 'Testing rule',
        ];

        $rule = rule::create_rule($data);
        $this->assertTrue($rule->valid_role());

        // Make the role invalid.
        $rule->roleid = $maxroleid + 1;
        $this->assertFalse($rule->valid_role());
    }
}
