<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

/**
 * Tests the \tool_roleremoval\messenger class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\messenger
 */
class messenger_test extends \advanced_testcase {
    /**
     * Tests sending of the notification message.
     */
    public function test_notify() {
        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('notifysignature', $signature, 'tool_roleremoval');

        $user = $this->getDataGenerator()->create_user(
            [
                'timezone' => 'Europe/London',
                'lastaccess' => 1598014800, // Fri Aug 21 2020 13:00:00 UCT.
            ]
        );

        $contexts = [
            [
                'name' => 'Course: Test 1',
                'url' => 'https://example.com/course/view.php?id=52',
            ],
        ];

        $revoketime = 1632229200; // Tue Sep 21 2021 13:00:00 UCT.

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();
        messenger::notify($user, $contexts, $revoketime, 'Manager');

        $this->assertEquals(1, $sink->count());
        $messages = $sink->get_messages();
        $message = current($messages);

        $this->assertEquals($user->id, $message->useridto);

        // The message is of the correct type.
        $this->assertEquals('tool_roleremoval', $message->component);
        $this->assertEquals('notify', $message->eventtype);

        // Check that the plain text message includes important information.
        $this->assertStringContainsString('Friday, 21 August 2020, 2:00 PM', $message->fullmessage);
        $this->assertStringContainsString('Tuesday, 21 September 2021, 2:00 PM', $message->fullmessage);
        $this->assertStringContainsString('Course: Test 1', $message->fullmessage);
        $this->assertStringContainsString('https://example.com/course/view.php?id=52', $message->fullmessage);
        $this->assertStringContainsString('Manager', $message->fullmessage);
        $this->assertStringContainsString($signature, $message->fullmessage);

        // Check that the html message includes important information.
        $this->assertStringContainsString('Friday, 21 August 2020, 2:00 PM', $message->fullmessagehtml);
        $this->assertStringContainsString('Tuesday, 21 September 2021, 2:00 PM', $message->fullmessagehtml);
        $this->assertStringContainsString('Course: Test 1', $message->fullmessagehtml);
        $this->assertStringContainsString('href="https://example.com/course/view.php?id=52"', $message->fullmessagehtml);
        $this->assertStringContainsString('Manager', $message->fullmessagehtml);
        $this->assertStringContainsString($signature, $message->fullmessagehtml);
    }

    /**
     * Tests sending of the revocation message.
     */
    public function test_revocation() {
        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('revocationsignature', $signature, 'tool_roleremoval');

        $user = $this->getDataGenerator()->create_user(
            [
                'timezone' => 'Europe/London',
                'lastaccess' => 1598014800, // Fri Aug 21 2020 13:00:00 UCT.
            ]
        );

        $contexts = [
            [
                'name' => 'Course: Test 1',
                'role' => 'Manager',
            ],
        ];

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();
        messenger::revocation($user, $contexts);

        $this->assertEquals(1, $sink->count());
        $messages = $sink->get_messages();
        $message = current($messages);

        $this->assertEquals($user->id, $message->useridto);

        // The message is of the correct type.
        $this->assertEquals('tool_roleremoval', $message->component);
        $this->assertEquals('revocation', $message->eventtype);

        // Check that the plain text message includes important information.
        $this->assertStringContainsString('Friday, 21 August 2020, 2:00 PM', $message->fullmessage);
        $this->assertStringContainsString('Course: Test 1', $message->fullmessage);
        $this->assertStringContainsString('Manager', $message->fullmessage);
        $this->assertStringContainsString($signature, $message->fullmessage);

        // Check that the html message includes important information.
        $this->assertStringContainsString('Friday, 21 August 2020, 2:00 PM', $message->fullmessagehtml);
        $this->assertStringContainsString('Course: Test 1', $message->fullmessagehtml);
        $this->assertStringContainsString('Manager', $message->fullmessagehtml);
        $this->assertStringContainsString($signature, $message->fullmessagehtml);
    }
}
