<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval;

use context_coursecat;
use moodle_url;

/**
 * Tests the \tool_roleremoval\export class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\export
 */
class export_test extends \advanced_testcase {
    /**
     * Tests that the user record is formatted correctly.
     */
    public function test_format_match_record() {
        global $CFG;

        $this->resetAfterTest(true);

        $CFG->showuseridentity = 'email,idnumber';

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        $course1 = $generator->create_course(['fullname' => 'A wonderful course', 'category' => $cat->id]);
        $inactivetime = time() - DAYSECS;

        // Users in the right area that have not logged in recently.
        $user1 = $generator->create_user(
            [
                'email' => 'joe.bloggs@example.com',
                'firstname' => 'Joe',
                'idnumber' => '123456789',
                'lastaccess' => $inactivetime,
                'lastname' => 'Bloggs',
            ]
        );
        $generator->enrol_user($user1->id, $course1->id, 'testingrole');

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(['contextid' => $catcontext->id, 'roleid' => $role, 'inactivefor' => HOURSECS]);

        $rule = rule::get_rule($rulerecord->id);

        $matches = $rule->get_matches();

        // Verify we got the single record we expected to.
        $this->assertCount(1, $matches);

        $match = current($matches);

        $url = new moodle_url('/course/view.php', ['id' => $course1->id]);

        $expected = [
            'fullname' => 'Joe Bloggs',
            'email' => 'joe.bloggs@example.com',
            'idnumber' => '123456789',
            'lastaccess' => userdate($inactivetime),
            'contextname' => 'Course: A wonderful course',
            'contexturl' => $url->out(false),
        ];

        $this->setAdminUser();
        $this->assertEquals($expected, export::format_match_record($match));
    }
}
