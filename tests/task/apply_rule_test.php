<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\task;

use context_coursecat;
use moodle_exception;
use tool_roleremoval\rule;

/**
 * Tests the \tool_roleremoval\task\apply_rule class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\task\apply_rule
 */
class apply_rule_test extends \advanced_testcase {
    /**
     * Clear the rule caches between runs.
     */
    protected function tearDown(): void {
        parent::tearDown();
        rule::reset();
    }

    /**
     * Tests that the task executes correctly.
     */
    public function test_execute() {
        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('notifysignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);
        $inactivetime = time() - DAYSECS;

        // Users in the right area that have not logged in recently.
        $user = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user->id, $catcontext->id);

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(
            [
                'contextid' => $catcontext->id,
                'roleid' => $role,
                'inactivefor' => HOURSECS,
                'enabled' => 0
            ]
        );

        // Run the task.
        $task1 = new apply_rule();
        $data1 = (object) array(
            'rule' => $rulerecord->id,
        );
        $task1->set_custom_data($data1);

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $cronoutput = "Processing rule: {$rulerecord->rulename}\n";

        $this->expectOutputString($cronoutput);
        $task1->execute();

        // Test messages were sent.
        $this->assertEquals(1, $sink->count());
    }

    /**
     * Tests that we cannot queue a rule for execution twice.
     */
    public function test_trigger() {
        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('notifysignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(['contextid' => $catcontext->id, 'roleid' => $role, 'inactivefor' => HOURSECS]);

        $rule = rule::get_rule($rulerecord->id);

        // This trigger should work fine.
        apply_rule::trigger($rule);

        // The second triggering should cause an exception.
        $this->expectException(moodle_exception::class);
        apply_rule::trigger($rule);
    }

    /**
     * Test that the task will finish if a rule has been deleted before the task executes.
     */
    public function test_rule_deleted() {
        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('notifysignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat = $generator->create_category([]);
        $catcontext = context_coursecat::instance($cat->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);
        $inactivetime = time() - DAYSECS;

        // Create and fetch the rule.
        $rulerecord = $generator->create_rule(
            [
                'contextid' => $catcontext->id,
                'roleid' => $role,
                'inactivefor' => HOURSECS,
                'enabled' => 0
            ]
        );

        // Users in the right area that have not logged in recently.
        $user = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user->id, $catcontext->id);

        // Create the task and then delete the rule.
        $task1 = new apply_rule();
        $data1 = (object) array(
            'rule' => $rulerecord->id,
        );
        $task1->set_custom_data($data1);

        $rule = rule::get_rule($rulerecord->id);
        $rule->delete();

        // Now test that the task runs without errors.
        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $cronoutput = "Rule {$rulerecord->id} has been deleted.\n";

        $this->expectOutputString($cronoutput);
        $task1->execute();

        // Test no messages were sent.
        $this->assertEquals(0, $sink->count());
    }
}
