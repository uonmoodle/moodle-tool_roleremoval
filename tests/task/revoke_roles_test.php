<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\task;

use context_coursecat;

/**
 * Tests the \tool_roleremoval\task\revoke_roles class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\task\revoke_roles
 */
class revoke_roles_test extends \advanced_testcase {
    /**
     * Tests that the task revokes roles from users as expected.
     */
    public function test_execute() {
        global $DB;

        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('revocationsignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat1 = $generator->create_category([]);
        $catcontext1 = context_coursecat::instance($cat1->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        $activetime = time() - DAYSECS;
        $inactivetime = time() - YEARSECS;

        $user1 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user1->id, $catcontext1->id);

        $user2 = $generator->create_user(['lastaccess' => $activetime]);
        $generator->role_assign($role, $user2->id, $catcontext1->id);

        $user3 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user3->id, $catcontext1->id);

        // Should be revoked.
        $generator->create_queue_entry($user1->id, $catcontext1->id, $role, []);

        // The user has logged in since the revocation was queued.
        $generator->create_queue_entry($user2->id, $catcontext1->id, $role, ['created' => $inactivetime - 1]);

        // Should not revoke this record yet.
        $generator->create_queue_entry($user3->id, $catcontext1->id, $role, ['revokeon' => time() + HOURSECS]);

        $records = $DB->count_records('tool_roleremoval_queue');

        $task = new revoke_roles();

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $cronoutput = "Cleaning queue... done.\nRevoking roles for user {$user1->id}... done.\n";

        $this->expectOutputString($cronoutput);
        $task->execute();

        // Tests that both successful and unneeded items are removed from the queue.
        $this->assertEquals($records - 2, $DB->count_records('tool_roleremoval_queue'));

        // Test messages were sent.
        $this->assertEquals(1, $sink->count());
    }

    /**
     * Test that if a context no longer exists that the task has no issues.
     */
    public function test_execute_missing_context() {
        global $DB;

        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('revocationsignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat1 = $generator->create_category([]);
        $catcontext1 = context_coursecat::instance($cat1->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        $inactivetime = time() - YEARSECS;

        $user1 = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user1->id, $catcontext1->id);

        $generator->create_queue_entry($user1->id, $catcontext1->id, $role, []);

        // Delete the context.
        $catcontext1->delete();

        $records = $DB->count_records('tool_roleremoval_queue');

        $task = new revoke_roles();

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $cronoutput = "Cleaning queue... done.\n";

        $this->expectOutputString($cronoutput);
        $task->execute();

        // Test messages were not sent.
        $this->assertEquals(0, $sink->count());

        // Tests that both successful and unneeded items are removed from the queue.
        $this->assertEquals($records - 1, $DB->count_records('tool_roleremoval_queue'));
    }

    /**
     * Test that if a role has been deleted that the task has no issues.
     */
    public function test_execute_missing_role() {
        global $DB;

        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('revocationsignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat1 = $generator->create_category([]);
        $catcontext1 = context_coursecat::instance($cat1->id);

        // Find the highest database id for a role.
        $roleids = array_keys(get_all_roles());
        asort($roleids, SORT_NUMERIC);
        $maxroleid = array_pop($roleids);

        $inactivetime = time() - YEARSECS;

        $user1 = $generator->create_user(['lastaccess' => $inactivetime]);

        $generator->create_queue_entry($user1->id, $catcontext1->id, $maxroleid + 1, []);

        $records = $DB->count_records('tool_roleremoval_queue');

        $task = new revoke_roles();

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $cronoutput = "Cleaning queue... done.\n";

        $this->expectOutputString($cronoutput);
        $task->execute();

        // Test messages were not sent.
        $this->assertEquals(0, $sink->count());

        // Tests that both successful and unneeded items are removed from the queue.
        $this->assertEquals($records - 1, $DB->count_records('tool_roleremoval_queue'));
    }
}
