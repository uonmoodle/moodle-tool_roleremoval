<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\task;

use context_coursecat;

/**
 * Tests the \tool_roleremoval\task\apply_rules class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\task\apply_rules
 */
class apply_rules_test extends \advanced_testcase {
    /**
     * Test that the task finds and executes rules.
     */
    public function test_exectute() {
        $this->resetAfterTest(true);

        $signature = 'Learning Technologies Team';
        set_config('notifysignature', $signature, 'tool_roleremoval');

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $cat1 = $generator->create_category([]);
        $catcontext1 = context_coursecat::instance($cat1->id);
        $cat2 = $generator->create_category([]);
        $catcontext2 = context_coursecat::instance($cat2->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);
        $inactivetime = time() - DAYSECS;

        // Users in the right area that have not logged in recently.
        $user = $generator->create_user(['lastaccess' => $inactivetime]);
        $generator->role_assign($role, $user->id, $catcontext1->id);
        $generator->role_assign($role, $user->id, $catcontext2->id);

        // Create and fetch the rule.
        $rulerecord1 = $generator->create_rule(
            [
                'contextid' => $catcontext1->id,
                'roleid' => $role,
                'inactivefor' => HOURSECS,
                'enabled' => 1
            ]
        );

        // This rule should not be run.
        $generator->create_rule(
            [
                'contextid' => $catcontext2->id,
                'roleid' => $role,
                'inactivefor' => HOURSECS,
                'enabled' => 0
            ]
        );

        $task = new apply_rules();

        $this->preventResetByRollback();
        $sink = $this->redirectMessages();

        $cronoutput = "Fetching rules... 1 rules found.\nProcessing rule: {$rulerecord1->rulename}\n";

        $this->expectOutputString($cronoutput);
        $task->execute();

        // Test messages were sent.
        $this->assertEquals(1, $sink->count());
    }
}
