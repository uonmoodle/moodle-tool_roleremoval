@tool @tool_roleremoval @uon
Feature: Role revocation message signature settings
  In order to easily maintain communications
  As an admin
  I need be able to configure message signatures

  Scenario: Edit signatures
    Given I log in as "admin"
    And I am on the "tool_roleremoval > Settings" page
    When I set the following fields to these values:
      | Notification message signature | Notifications team |
      | Revocation message signature | Revocation hit squad |
    And I press "Save changes"
    And I follow "Preview messages"
    Then I should see "Notifications team"
    And I should see "Revocation hit squad"
