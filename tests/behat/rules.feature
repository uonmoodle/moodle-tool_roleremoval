@tool @tool_roleremoval @uon
Feature: Role revocation rules
  In order to meet GDPR role audit requirements
  As an admin
  I need be able to define rules that will remove roles from users hwo have not accessed Moodle

  @javascript
  Scenario: Create a rule
    Given I log in as "admin"
    And I am on the "tool_roleremoval > Rule management" page
    When I follow "Create rule"
    And I set the following fields to these values:
      | Name | My little test rule |
      | Automatic rule | 1 |
      | Role | Manager |
      | inactivefor[number] | 28 |
      | inactivefor[timeunit] | days |
      | notice[number] | 2 |
      | notice[timeunit] | weeks |
      | Context | Category 1 |
    And I press "Save changes"
    Then I should see "Rule saved"
    And I should see "Category: Category 1" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Manager" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Users must be inactive for 28 days" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Users given 14 days to login" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Automatic rule" in the "My little test rule" "tool_roleremoval > Rule"
    But I should not see "Manual rule" in the "My little test rule" "tool_roleremoval > Rule"

  @javascript
  Scenario: Edit a rule
    Given the following "tool_roleremoval > rule" exists:
      | rulename | My little test rule |
      | enabled  | 1 |
      | role | manager |
      | context | System |
      | contextlevel | System |
    And I log in as "admin"
    And I am on the "tool_roleremoval > Rule management" page
    When I click on "Edit" "link_or_button" in the "My little test rule" "tool_roleremoval > Rule"
    And I set the following fields to these values:
      | Name | My little test rule |
      | Automatic rule | 0 |
      | Role | Course creator |
      | inactivefor[number] | 10 |
      | inactivefor[timeunit] | days |
      | notice[number] | 2 |
      | notice[timeunit] | weeks |
      | Context | Category 1 |
    And I press "Save changes"
    Then I should see "Rule saved"
    And I should see "Category: Category 1" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Course creator" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Users must be inactive for 10 days" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Users given 14 days to login" in the "My little test rule" "tool_roleremoval > Rule"
    And I should see "Manual rule" in the "My little test rule" "tool_roleremoval > Rule"
    But I should not see "Automatic rule" in the "My little test rule" "tool_roleremoval > Rule"

  Scenario: Delete a rule
    Given the following "category" exists:
      | name | Test category |
      | idnumber | testcat  |
    And the following "tool_roleremoval > rule" exists:
      | rulename | My little test rule |
      | enabled | 0 |
      | role | coursecreator |
      | context | testcat |
      | contextlevel | Category |
    And I log in as "admin"
    And I am on the "tool_roleremoval > Rule management" page
    When I click on "Delete" "link_or_button" in the "My little test rule" "tool_roleremoval > Rule"
    And I press "Continue"
    Then I should see "Rule \"My little test rule\" was deleted"
    And "My little test rule" "tool_roleremoval > Rule" should not exist

  Scenario: View users who will be affected by a rule
    Given the following "users" exist:
      | username | firstname | lastname | email            | lastaccess |
      | user1    | Joe       | Bloggs   | joe@example.com  | ##1 year ago## |
      | user2    | Jane      | Doe      | jane@example.com | ##1 year ago## |
      | user3    | John      | Smith    | john@example.com | ##1 day ago##  |
    And the following "category" exists:
      | name | Test category |
      | idnumber | testcat  |
    And the following "role assigns" exist:
      | user | role | contextlevel | reference |
      | user1 | manager | Category | testcat   |
      | user2 | manager | Category | testcat   |
      | user3 | manager | Category | testcat   |
    # The inactive for value is 1 week in seconds.
    And the following "tool_roleremoval > rule" exists:
      | rulename | My little test rule |
      | enabled  | 0 |
      | role | manager |
      | context | System |
      | contextlevel | System |
      | inactivefor  | 604800 |
    And the following "tool_roleremoval > queued revocation" exists:
      | user | user2 |
      | context | testcat |
      | contextlevel | Category |
      | role | manager  |
    And I log in as "admin"
    And I am on the "tool_roleremoval > Rule management" page
    When I click on "View matching users" "link_or_button" in the "My little test rule" "tool_roleremoval > Rule"
    Then I should see "Joe Bloggs"
    But I should not see "Jane Doe"
    And I should not see "John Smith"

  Scenario: Trigger a manual rule
    Given the following "category" exists:
      | name | Test category |
      | idnumber | testcat  |
    And the following "tool_roleremoval > rule" exists:
      | rulename | My little test rule |
      | enabled  | 0 |
      | role | manager |
      | context | System |
      | contextlevel | System |
    And I log in as "admin"
    And I am on the "tool_roleremoval > Rule management" page
    And I click on "View matching users" "link_or_button" in the "My little test rule" "tool_roleremoval > Rule"
    When I follow "Run rule"
    And I press "Continue"
    Then I should see "The rule has been queued to run"
