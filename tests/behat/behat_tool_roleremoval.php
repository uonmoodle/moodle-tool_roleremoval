<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(__DIR__ . '/../../../../../lib/behat/behat_base.php');

/**
 * Behat steps definitions.
 *
 * @package     tool_roleremoval
 * @category    test
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class behat_tool_roleremoval extends behat_base {
    /**
     * Recognised named selectors:
     * | Rule           | A rule card on the rule management page.             |
     *
     * @see behat_base::get_exact_named_selectors()
     */
    public static function get_exact_named_selectors(): array {
        $rule = new behat_component_named_selector(
            'Rule',
            [
                "//*[%tool_roleremoval/ruleClass% and .//*[%tool_roleremoval/cardHeader% and .//text()[contains(., %locator%)]]]",
            ]
        );

        // Return the selectors.
        return [
            $rule,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function get_named_replacements(): array {
        // First list locators with no dependencies.
        $ruleclass = new behat_component_named_replacement(
            'ruleClass',
            "contains(concat(' ', normalize-space(@class), ' '), ' tool-roleremoval-rule ')"
        );

        // First list locators with no dependencies.
        $cardheader = new behat_component_named_replacement(
            'cardHeader',
            "contains(concat(' ', normalize-space(@class), ' '), ' card-header ')"
        );

        return [
            $ruleclass,
            $cardheader,
        ];
    }

    /**
     * Recognised page names are:
     * | Rule management | Administration page for rules         |
     * | Settings        | The settings page for the plugin      |
     *
     * @see behat_base::resolve_page_url()
     */
    protected function resolve_page_url(string $page): moodle_url {
        switch ($page) {
            case 'Rule management':
                $url = new moodle_url('/admin/tool/roleremoval/index.php');
                break;
            case 'Settings':
                $url = new moodle_url('/admin/settings.php', ['section' => 'tool_roleremoval']);
                break;
            default:
                throw new Exception("tool_roleremoval does not recognise the '$page' type");
        }

        return $url;
    }
}
