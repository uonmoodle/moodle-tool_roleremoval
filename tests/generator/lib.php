<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use tool_roleremoval\rule;


/**
 * The tool_roleremoval data generator.
 *
 * @package    tool_roleremoval
 * @copyright  2021 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class tool_roleremoval_generator extends testing_data_generator {
    /** @var int A count of the number of rules created by the generator. */
    protected static $rulescreated = 0;

    /**
     * Creates a rule in the database.
     *
     * @param array $record
     * @return \stdClass
     * @throws \coding_exception
     */
    public function create_rule(array $record): stdClass {
        if (!isset($record['contextid'])) {
            throw new coding_exception('You must include contextid');
        }

        if (!isset($record['roleid'])) {
            throw new coding_exception('You must include roleid');
        }

        $defaults = [
            'enabled' => true,
            'inactivefor' => YEARSECS,
            'notice' => WEEKSECS * 4,
            'rulename' => 'Rule ' . static::$rulescreated++,
        ];

        $record = $this->combine_defaults_and_record($defaults, $record);

        // Ensure that it will be a new rule.
        $record['id'] = null;

        $rule = rule::create_rule((object) $record);
        $rule->save();

        $data = (object) (array) $rule;
        $data->id = $rule->get_id();

        return $data;
    }

    /**
     * Creates a record for a role to be revoked.
     *
     * The extra data can include:
     * - revokeon: timestamp defaults to 1 second ago
     * - created: timestamp defaults to 4 weeks ago
     *
     * @param int $user The id of the user
     * @param int $context The id of a context
     * @param int $role The id of a role
     * @param array $record Extra data
     * @return \stdClass
     */
    public function create_queue_entry(int $user, int $context, int $role, array $record): stdClass {
        global $DB;

        $record['contextid'] = $context;
        $record['userid'] = $user;
        $record['roleid'] = $role;

        $defaults = [
            'revokeon' => time() - 1, // Will revoke from now.
            'created' => time() - (WEEKSECS * 4), // Was created 4 weeks ago.
        ];

        $record = (object) $this->combine_defaults_and_record($defaults, $record);

        $record->id = $DB->insert_record('tool_roleremoval_queue', $record);

        return $record;
    }
}
