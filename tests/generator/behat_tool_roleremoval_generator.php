<?php
// This file is part of University of Nottingham enrolment plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Maps data generation methods for use in behat.
 *
 * @package    enrol_nottingham
 * @category   test
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2020 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class behat_tool_roleremoval_generator extends behat_generator_base {
    /**
     * @inheritDoc
     */
    protected function get_creatable_entities(): array {
        return [
            'rules' => [
                'singular' => 'rule',
                'datagenerator' => 'rule',
                'required' => ['context', 'contextlevel', 'role'],
                'switchids' => ['role' => 'roleid'],
            ],
            'queued revocations' => [
                'singular' => 'queued revocation',
                'datagenerator' => 'revocation',
                'required' => ['user', 'context', 'contextlevel', 'role'],
                'switchids' => ['role' => 'roleid', 'user' => 'userid'],
            ],
        ];
    }

    /**
     * Prepares the behat values for entry into the data generator.
     *
     * It converts the context into a contextid.
     *
     * @param array $entry
     * @return array
     */
    protected function preprocess_rule(array $entry): array {
        // Get the context for the rule.
        $context = $this->get_context($entry['contextlevel'], $entry['context']);
        unset($entry['contextlevel']);
        unset($entry['context']);
        $entry['contextid'] = $context->id;

        return $entry;
    }

    /**
     * Creates a revocation queue entry.
     *
     * @param array $entry
     * @retrun void
     */
    protected function process_revocation(array $entry) {
        // Get the context for the revocation.
        $context = $this->get_context($entry['contextlevel'], $entry['context']);
        unset($entry['contextlevel']);
        unset($entry['context']);
        $entry['contextid'] = $context->id;

        $this->componentdatagenerator->create_queue_entry($entry['userid'], $context->id, $entry['userid'], $entry);
    }
}
