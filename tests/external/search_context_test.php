<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\external;

use core_external\external_api;

/**
 * Tests the \tool_roleremoval\external\search_context class.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\external\search_context
 * @runTestsInSeparateProcesses
 */
class search_context_test extends \advanced_testcase {
    /**
     * Tests that the web service returns results as expected.
     *
     * @param array $args The arguments to be passed to the web service.
     * @dataProvider data_get
     */
    public function test_get(array $args, int $expectedresults) {
        global $CFG, $USER;

        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');
        $category1 = $generator->create_category(['name' => 'Red']);
        $category2 = $generator->create_category(['name' => 'Blue']);
        $category3 = $generator->create_category(['name' => 'Green']);

        $generator->create_course(['fullname' => 'Red']);
        $generator->create_course(['fullname' => 'Blue', 'category' => $category1->id]);
        $generator->create_course(['fullname' => 'Green', 'category' => $category3->id]);
        $generator->create_course(['fullname' => 'Yellow']);
        $generator->create_course(['fullname' => 'Blurple', 'category' => $category2->id]);

        $this->setAdminUser();
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $result = external_api::call_external_function('tool_roleremoval_search_context', $args, true);

        $this->assertFalse($result['error']);
        $this->assertCount($expectedresults, $result['data']['contexts']);
    }

    /**
     * Data provider for test_get.
     * @return array
     */
    public function data_get(): array {
        return [
            'emptyquery' => [
                ['query' => ''],
                1,
            ],
            'textquery' => [
                ['query' => 'lu'],
                3,
            ],
            'limit' => [
                ['query' => 'lu', 'limit' => 1],
                1,
            ],
        ];
    }

    /**
     * Test that a user without the required capabilities cannot user the web service.
     */
    public function test_get_unprivillaged() {
        global $CFG;

        $this->resetAfterTest(true);

        $user = $this->getDataGenerator()->create_user();
        // Do not require a session key via POST, so that the calls will not error.
        $user->ignoresesskey = true;
        $this->setUser($user);

        $result = external_api::call_external_function('tool_roleremoval_search_context', ['query' => '']);

        $this->assertTrue($result['error']);
        $this->assertEquals('nopermissions', $result['exception']->errorcode);
    }
}
