<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_roleremoval\privacy;

use context_course;
use context_coursecat;
use core_privacy\local\request\approved_userlist;
use core_privacy\local\request\userlist;
use core_privacy\tests\request\approved_contextlist;
use core_user;

/**
 * Tests the privacy API implementation.
 *
 * @package     tool_roleremoval
 * @copyright   2021 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group tool_roleremoval
 * @group uon
 * @covers \tool_roleremoval\privacy\provider
 */
class provider_test extends \core_privacy\tests\provider_testcase {
    /**
     * Tests that we get the context the user is part of.
     */
    public function test_get_contexts_for_userid() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');

        // Setup some data.
        $user = $generator->create_user();
        $otheruser = $generator->create_user();
        $course = $generator->create_course();
        $context = context_course::instance($course->id);
        $othercourse = $generator->create_course();
        $othercontext = context_course::instance($othercourse->id);
        $category = $generator->create_category();
        $catcontext = context_coursecat::instance($category->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        // Create some entries that should be found.
        $generator->create_queue_entry($user->id, $context->id, $role, []);
        $generator->create_queue_entry($user->id, $catcontext->id, $role, []);

        // Create some entries that should not be found.
        $generator->create_queue_entry($otheruser->id, $context->id, $role, []);
        $generator->create_queue_entry($otheruser->id, $othercontext->id, $role, []);

        $contextlist = $this->get_contexts_for_userid($user->id, 'tool_roleremoval');
        $contexts = $contextlist->get_contextids();

        $this->assertCount(2, $contexts);
        $this->assertContainsEquals($context->id, $contexts);
        $this->assertContainsEquals($catcontext->id, $contexts);
    }

    /**
     * Ensure that all user data is deleted from a context.
     */
    public function test_all_users_deleted_from_context() {
        global $DB;
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');

        // Setup some data.
        $user = $generator->create_user();
        $otheruser = $generator->create_user();
        $course = $generator->create_course();
        $context = context_course::instance($course->id);
        $category = $generator->create_category();
        $catcontext = context_coursecat::instance($category->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        // Create some entries that should be deleted.
        $generator->create_queue_entry($user->id, $context->id, $role, []);
        $generator->create_queue_entry($otheruser->id, $context->id, $role, []);

        // Create some entries that should not be deleted.
        $generator->create_queue_entry($user->id, $catcontext->id, $role, []);

        provider::delete_data_for_all_users_in_context($context);

        // Test that the intended records were removed.
        $delparams = ['contextid' => $context->id];
        $this->assertEquals(0, $DB->count_records('tool_roleremoval_queue', $delparams));

        // Test that data from other contexts were not removed.
        $this->assertEquals(1, $DB->count_records('tool_roleremoval_queue', ['contextid' => $catcontext->id]));
    }

    /**
     * Tests that data can be deleted for one user on a context.
     */
    public function test_delete_data_for_user() {
        global $DB;
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');

        // Setup some data.
        $user = $generator->create_user();
        $otheruser = $generator->create_user();
        $course = $generator->create_course();
        $context = context_course::instance($course->id);
        $category = $generator->create_category();
        $catcontext = context_coursecat::instance($category->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        // Create some entries that should be deleted.
        $generator->create_queue_entry($user->id, $context->id, $role, []);

        // Create some entries that should not be deleted.
        $generator->create_queue_entry($user->id, $catcontext->id, $role, []);
        $generator->create_queue_entry($otheruser->id, $context->id, $role, []);

        $approvedcontextlist = new approved_contextlist(
            core_user::get_user($user->id),
            'tool_roleremoval',
            [$context->id]
        );
        provider::delete_data_for_user($approvedcontextlist);

        // Test that the intended record was removed.
        $delparams = ['userid' => $user->id, 'contextid' => $context->id];
        $this->assertEquals(0, $DB->count_records('tool_roleremoval_queue', $delparams));

        // Test that other users on the context were not removed.
        $this->assertEquals(1, $DB->count_records('tool_roleremoval_queue', ['contextid' => $context->id]));

        // Test that other records for the user were not deleted.
        $this->assertEquals(1, $DB->count_records('tool_roleremoval_queue', ['userid' => $user->id]));
    }

    /**
     * Test that all users with data in the context can be found.
     */
    public function test_get_users_in_context() {
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');

        // Setup some data.
        $user1 = $generator->create_user();
        $user2 = $generator->create_user();
        $otheruser = $generator->create_user();
        $course = $generator->create_course();
        $context = context_course::instance($course->id);
        $category = $generator->create_category();
        $catcontext = context_coursecat::instance($category->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        // Create some entries that should be found.
        $generator->create_queue_entry($user1->id, $context->id, $role, []);
        $generator->create_queue_entry($user2->id, $context->id, $role, []);

        // Create some entries that should not be found.
        $generator->create_queue_entry($user1->id, $catcontext->id, $role, []);
        $generator->create_queue_entry($otheruser->id, $catcontext->id, $role, []);

        $userlist = new userlist($context, 'tool_roleremoval');
        provider::get_users_in_context($userlist);
        $userids = $userlist->get_userids();

        $this->assertCount(2, $userids);
        $this->assertContainsEquals($user1->id, $userids);
        $this->assertContainsEquals($user2->id, $userids);
    }

    /**
     * Test that all data for a selected list of users in a context are deleted.
     */
    public function test_delete_data_for_users() {
        global $DB;
        $this->resetAfterTest(true);

        /* @var \tool_roleremoval_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('tool_roleremoval');

        // Setup some data.
        $user = $generator->create_user();
        $otheruser = $generator->create_user();
        $course = $generator->create_course();
        $context = context_course::instance($course->id);
        $category = $generator->create_category();
        $catcontext = context_coursecat::instance($category->id);
        $role = $generator->create_role(['shortname' => 'testingrole']);

        // Create some entries that should be deleted.
        $generator->create_queue_entry($user->id, $context->id, $role, []);

        // Create some entries that should not be deleted.
        $generator->create_queue_entry($otheruser->id, $context->id, $role, []);
        $generator->create_queue_entry($user->id, $catcontext->id, $role, []);

        $userlist = new approved_userlist($context, 'tool_roleremoval', [$user->id]);
        provider::delete_data_for_users($userlist);

        // Test that the intended record was removed.
        $delparams = ['userid' => $user->id, 'contextid' => $context->id];
        $this->assertEquals(0, $DB->count_records('tool_roleremoval_queue', $delparams));

        // Test that other users on the context were not removed.
        $this->assertEquals(1, $DB->count_records('tool_roleremoval_queue', ['contextid' => $context->id]));

        // Test that other records for the user were not deleted.
        $this->assertEquals(1, $DB->count_records('tool_roleremoval_queue', ['userid' => $user->id]));
    }
}
