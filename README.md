ABOUT
==========
The 'Role revocation' tool was developed by
* Neill Magill

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The tool is designed to allow admins to configure Moodle to revoke roles from users who have not logged
in for an excessive amount of time.

INSTALLATION
==========
The role revocation tool follows the standard installation procedure.

1. Create folder <path to your moodle dir>/admin/tool/roleremoval
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.

CONFIGURATION
==========
After installation the rules used for role revocation can be configured by visiting:
Home ► Site administration ► Plugins ► Admin tools ► Role revocation

Rules can be configured by visiting:
Home ► Site administration ► Users ► Permissions ► Role revocation
